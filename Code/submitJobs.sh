OUTDIR="/ptmp/mpp/dduda/Charged_Higgs_Code/chargedhiggstowhframework/ReadEasyTrees"
USE_BATCH_MODE=1

WP=(
#"77p"
"70p"
#"85p"
#"60p"
)

Paths=(
"inputs/1Lep/MC16a/"
"inputs/1Lep/MC16d/"
"inputs/1Lep/MC16e/"
)

File=(
diboson.root
sig_Hplus_Wh_m400-0.root
sig_Hplus_Wh_m800-0.root
sig_Hplus_Wh_m1600-0.root
ttbar.root
Wjets.root
singleTop.root
)

rm -rf cluster_pack.tar.gz
tar -czf cluster_pack.tar.gz Makefile_batch main_batch.C  main_RunMVATraining.C dataset/ main/ TH1Fs/ utilis/ LatexOutput/ python/ style/

for wp in "${WP[@]}"
do
   for path in "${Paths[@]}"
   do
      for file in "${File[@]}"
      do
      ##./execute $path $file $wp $OUTDIR $USE_BATCH_MODE
      sbatch RunJob.sh $path $file $wp $OUTDIR $USE_BATCH_MODE
      done
   done
done
echo "all done !!! "

