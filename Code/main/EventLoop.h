
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Nov 15 13:27:20 2019 by ROOT version 6.18/00
// from TTree Nominal/Nominal
// found on file: ../my_analysis_dir/run/submitDir_HIGG5D2_1L-MC16a-HVT-32-14-TCC-Wplus-2019-11-15_12h09/data-EasyTree/ttbar_nonallhad_PwPy8.root
//////////////////////////////////////////////////////////

#ifndef EventLoop_h
#define EventLoop_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"
#include "TLorentzVector.h"
#include "vector"
#include <iostream>

#include "utilis/Chi2_minimization.h"	
#include "utilis/NeutrinoBuilder.h"
#include "TLorentzVector.h"
#include "TH1F.h"
#include <stdio.h>
#include <chrono>
#include <time.h>
#include "TH1Fs/TH1Fs.h"
#include "TMVA/Reader.h"

class EventLoop {
public :
   TTree           *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   TTree           *m_myTree_hadronic; //!pointer to the output TTree 
   TTree           *m_myTree_Leptonic; //!pointer to the output TTree
   TTree           *m_myTree_Event; //!pointer to the output TTree
   TMVA::Reader    *m_reader_qqbb;  //!
   TMVA::Reader    *m_reader_lvbb;  //!
   TTree           *Nom;

   // Fixed size dimensions of array or collections stored in the TTree if any.
   
   // Declaration of leaf types
   Int_t           nJet;
   //Int_t           nFatJets;
   clock_t start =    0;
   clock_t end   = 0;
   double cpu_time_used = 0;
   Int_t           mcChannelNumber;
   Int_t           TopHeavyFlavorFilterFlag;
   Int_t           nMuons;
   Int_t           nElectrons;
   Long_t          eventNumber;
   Int_t           nPrimaryVtx;
   Int_t           RunNumber;
   Int_t           randomRunNumber;
   Int_t           isTriggered;
   Int_t           nTaus;
   Int_t           HF_SimpleClassification;
   Int_t           jet_truthflav;   //only for MC
   ////Int_t           hasBadMuon;
   bool            HLT_mu26_ivarmedium;
   bool            HLT_mu50;
   bool            HLT_e60_lhmedium_nod0;
   bool            HLT_e140_lhloose_nod0;
   bool            HLT_e26_lhtight_nod0_ivarloose;
   Float_t         Mu;
   ////Float_t         ActualMu;
   Float_t         EventWeight;

   //The weights given below are only for MC
   
   Float_t         weight_normalise;
   Float_t         weight_pileup;
   Float_t         weight_mc;
   Float_t         weight_leptonSF;
   Float_t         weight_bTagSF_DL1r_Continuous;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_up;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_down;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_up;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_down;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;   
   Float_t         weight_normalise_fsr_down;
   Float_t         weight_normalise_fsr_up;  
   Float_t         weight_normalise_muR05_muF05;
   Float_t         weight_normalise_muR20_muF20;
   Float_t         weight_normalise_var3c_down;
   Float_t         weight_normalise_var3c_up;

   Float_t         weight_fsr_down;
   Float_t         weight_fsr_up;  
   Float_t         weight_muR05_muF05;
   Float_t         weight_muR20_muF20;
   Float_t         weight_var3c_down;
   Float_t         weight_var3c_up;  
   Float_t         weight_jvt; 
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   
   //muon weights
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;

   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;

   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;

   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   
   //electron weights
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;

   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;

   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;

   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN; 
   
   Float_t         met;
   Float_t         met_phi;
   Float_t         dRbb_HiggsMass_85;
   Float_t         dRbb_MaxM_85;
   Float_t         dRbb_MaxPt_85;
   Float_t         dRbb_MindR_85;
   Float_t         dRbb_avg_85;
   Float_t         dRbj_Wmass_85;
   Float_t         Aplanarity_bjets_85;
   Float_t         Aplanarity_jets;
   Float_t         Centrality_all;
   Float_t         Mbj_MaxPt_85;
   Float_t         pT_jet3;
   Float_t         pT_jet5;
   Float_t         pTbb_MindR_85;
   Float_t         pTuu_MindR_85;
   Float_t         Mbb_MindR_85;
   Float_t         Mbb_MinM_85;
   Float_t         Mbb_MaxPt_85;
   Float_t         Mbb_MaxM_85;
   Float_t         HT_all;
   //std::vector<float>   *mc_generator_weights;
   std::vector<float>   *el_pt;
   std::vector<float>   *el_eta;
   std::vector<float>   *el_phi;
   std::vector<float>   *el_e;
   //std::vector<float>   *el_cl_eta;
   std::vector<float>   *el_charge;
   /*std::vector<float>   *el_topoetcone20;
   std::vector<float>   *el_ptvarcone20;
   std::vector<float>   *el_isTight;
   std::vector<float>   *el_CF;
   std::vector<float>   *el_d0sig;
   std::vector<float>   *el_delta_z0_sintheta;
   std::vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   std::vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   std::vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   std::vector<char>    *el_LHMedium;
   std::vector<char>    *el_LHTight;
   std::vector<char>    *el_isPrompt;
   std::vector<char>    *el_isoFCLoose;
   std::vector<char>    *el_isoFCTight;*/
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_e;
   std::vector<float>   *mu_charge;
   /*std::vector<float>   *mu_d0sig;
   std::vector<float>   *mu_delta_z0_sintheta;
   std::vector<char>    *mu_trigMatch_HLT_mu50;
   std::vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   std::vector<char>    *mu_Medium; 
   std::vector<char>    *mu_isoFCTightTrackOnly;*/
   /*std::vector<float>   *FatJet_M;
   std::vector<float>   *FatJet_PT;
   std::vector<float>   *FatJet_Eta;
   std::vector<float>   *FatJet_Phi;*/
   std::vector<float>   *signal_Jet_E;
   std::vector<float>   *signal_Jet_PT;
   std::vector<float>   *signal_Jet_Eta;
   std::vector<float>   *signal_Jet_Phi;
   std::vector<int>   *signal_Jet_truthflav;  //only for MC
   std::vector<int>     *signal_Jet_tagWeightBin_DL1r_Continuous;
   
   // Given below variables only for MC
   std::vector<int>     *truth_pdgid;
   std::vector<int>     *truth_status;
   ////std::vector<int>     *truth_barcode;
   std::vector<float>   *truth_pt; 
   std::vector<float>   *truth_eta; 
   std::vector<float>   *truth_phi; 
   std::vector<float>   *truth_m;
   std::vector<long>   *truth_tthbb_info; 

   /*std::vector<float>   *TrackJet_PT;
   std::vector<float>   *TrackJet_Eta;
   std::vector<float>   *TrackJet_Phi;
   std::vector<float>   *TrackJet_M;
   std::vector<float>   *TrackJet_btagWeight;*/
   std::vector<TLorentzVector*> NeutrinoVec;
   map<TString, bool> pass_sel;

   // List of branches
   TBranch        *b_nJet;   //!
   //TBranch        *b_nFatJets;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_nMuons;   //!
   TBranch        *b_nElectrons;   //!
   TBranch        *b_nPrimaryVtx;   //!
   TBranch        *b_nTaus;   //!
   TBranch        *b_HF_SimpleClassification; //!
   TBranch        *b_mcChannelNumber;
   TBranch        *b_TopHeavyFlavorFilterFlag;
   ////TBranch        *b_hasBadMuon;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_Mu;   //!
   ////TBranch        *b_ActualMu;   //!
   ////TBranch        *b_weight_normalise;   //!
   
   //The weights given below are only for MC
   
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_normalise;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_jvt;   //! 
   TBranch        *b_weight_pileup_UP; //!
   TBranch        *b_weight_pileup_DOWN; //!
   TBranch        *b_weight_jvt_UP;      //!
   TBranch        *b_weight_jvt_DOWN;    //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up;
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down;
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up;
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down;
   TBranch        *b_weight_normalise_fsr_down;
   TBranch        *b_weight_normalise_fsr_up;
   TBranch        *b_weight_normalise_muR05_muF05;
   TBranch        *b_weight_normalise_muR20_muF20;
   TBranch        *b_weight_normalise_var3c_down;
   TBranch        *b_weight_normalise_var3c_up;
   TBranch        *b_weight_fsr_down;
   TBranch        *b_weight_fsr_up;
   
   TBranch        *b_weight_muR05_muF05;
   TBranch        *b_weight_muR20_muF20;
   TBranch        *b_weight_var3c_down;
   TBranch        *b_weight_var3c_up; 
   
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;

   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;

   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;

   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;

   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;

   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;
  
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;

   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;
   
   
   TBranch        *b_MET;   //!
   TBranch        *b_MET_phi;   //!
   TBranch        *b_dRbb_HiggsMass_85;   //!
   TBranch        *b_dRbb_MaxM_85;   //!
   TBranch        *b_dRbb_MaxPt_85;   //!
   TBranch        *b_dRbb_MindR_85;   //!
   TBranch        *b_dRbb_avg_85;   //!
   TBranch        *b_dRbj_Wmass_85;   //!
   TBranch        *b_Aplanarity_bjets_85;   //!
   TBranch        *b_Aplanarity_jets;   //!
   TBranch        *b_Centrality_all;   //!
   TBranch        *b_Mbj_MaxPt_85;   //!
   TBranch        *b_pT_jet3;   //!
   TBranch        *b_pT_jet5;   //!
   TBranch        *b_pTbb_MindR_85;   //!
   TBranch        *b_pTuu_MindR_85;   //!
   TBranch        *b_Mbb_MindR_85;   //!
   TBranch        *b_Mbb_MinM_85;   //!
   TBranch        *b_Mbb_MaxPt_85;   //!
   TBranch        *b_Mbb_MaxM_85;   //!
   TBranch        *b_HT_all;

   //TBranch        *b_mc_generator_weights;   //!  
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
  // TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_charge;   //!
   /*TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_isTight;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_LHMedium;   //!
   TBranch        *b_el_LHTight;   //!
   TBranch        *b_el_isPrompt;   //!
   TBranch        *b_el_isoFCLoose;   //!
   TBranch        *b_el_isoFCTight;   //! */
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   /*TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_mu_Medium;   //!
   TBranch        *b_mu_isoFCTightTrackOnly;   //!*/
   //TBranch        *b_FatJet_M;   //!
   //TBranch        *b_FatJet_PT;   //!
   //TBranch        *b_FatJet_Eta;   //!
   //TBranch        *b_FatJet_Phi;   //!
   TBranch        *b_signal_Jet_E;   //!
   TBranch        *b_signal_Jet_PT;   //!
   TBranch        *b_signal_Jet_Eta;   //!
   TBranch        *b_signal_Jet_Phi;   //!
   TBranch        *b_signal_Jet_truthflav;   //!   //only for MC
   TBranch        *b_signal_Jet_tagWeightBin_DL1r_Continuous;   //!
   
   //only for MC
   
   TBranch        *b_truth_pt;   //!
   TBranch        *b_truth_eta;   //!
   TBranch        *b_truth_phi;   //!
   TBranch        *b_truth_m;   //!
   TBranch        *b_truth_pdgid;   //! 
   TBranch        *b_truth_status;   //!
   TBranch        *b_truth_tthbb_info; //!  
   

   //TBranch        *b_truth_barcode;   //!
   //TBranch        *b_TrackJet_PT;   //!
   //TBranch        *b_TrackJet_Eta;   //!
   //TBranch        *b_TrackJet_Phi;   //!
   //TBranch        *b_TrackJet_M;   //!
   //TBranch        *b_TrackJet_btagWeight;   //!

   EventLoop(TTree *tree=0, TString sampleName="", TString MCDataPeriode="", TString ExpUncertaintyName="Nominal", TString WP="", TString TOPCON="");
   void Write(TDirectory *dir, std::string dirname);
   void WriteTreesToFile(TFile *outFile);
   void FillMVATree(TLorentzVector W);
   float CosThetaStar(TLorentzVector higgs, TLorentzVector w);
   void FillMVATree(int i_H1, int i_H2, int i_w1, int i_w2, bool is_signal);
   void FillMVATreeLeptonic(int i_H1,int i_H2, TLorentzVector W, bool is_signal);
   void	Sort_Jets(std::vector<TLorentzVector> *Jets, std::vector<int> *is_tagged);
   void Set_Jet_observables();
   void SetTruthParticles();
   void SetTruthParticles_tthbb();
   //different SetTruthParticles variant for BDT training
   void SetTruthParticles_TruthJets(); //instead of partons use truth jets
   void SetTruthParticlesMatchTruthJets();//match partons to jets
   void SetTruthParticlesMatchPartonJets();

   void SetJetVectors();
   void SetLeptonVectors();
   void WriteMVAInput();
   void MatchTruthParticlesToJets();
   void MatchTruthQuarksToJets();
   void MatchTruthQuarksToJets_Match();
   void MatchTruthParticleToJetsLeptonic();
   void initializeMVA_qqbb();
   void initializeMVA_lvbb();
   void dRjj();
   bool Findhad_tt();
   void Calc_dR_jl(float &dR_jl_min, float &dR_jl_max);
   double GetMwt();
   double GetTruthMass();
   double EvaluateMVAResponse_qqbb(int i_H1, int i_H2, int i_w1, int i_w2);
   double EvaluateMVAResponse_lvbb(int i_H1, int i_H2, TLorentzVector W);
   bool FindJetPair_qqbb();

   void Accuracy_qqbb();

   bool FindJetPair_qqbb_true();
   bool FindJetPair_lvbb();
   void GetTruthTop();
   //bool FindFJetPair();
   bool PassEventSelectionResolved();
   bool PassEventSelectionResolved_topreco();
   bool PassEventSelectionResolved_VR();
   //bool PassEventSelectionBoosted();
   int GetBTagCategory(int NTags_InHiggsJet, int NTags_OutsideHiggsJet);
   //int GetBTagCategoryShort(int NTags_InHiggsJet, int NTags_OutsideHiggsJet);
   int GetBTagCategoryShort(int NTags_InHiggsJet, int NTags_OutsideHiggsJet);
   int GetBTagCategory_PreSel();
   int GetTagWeightBin(double btag_score);
   TLorentzVector GetWBoson(bool &status);
   TLorentzVector BuildLeptonicTop();
   bool GetLeptonicTop();
   //bool GetLeptonicTop_minpT();
   double GetLeptonicTop_minpT();
   //bool GetLeptonicTop_minXwT();
   //bool GetLeptonicTop_min();
   //bool GetLeptonicTop_minX();
   double GetLeptonicTop_minX();
   double GetLeptonicTop_minXwT();
   double RWFunc(double HT, double nJets);
   std::vector<TLorentzVector*> GetNeutrinos(TLorentzVector* L, TLorentzVector* MET);
   virtual ~EventLoop();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree, TString sampleName, TString ExpUncertaintyName);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

    
   Chi2_minimization* myMinimizer = new Chi2_minimization("MeV");
   NeutrinoBuilder* m_NeutrinoBuilder;
   
   TH1Fs *h_NBtags;
   TH1Fs *h_MET;
   TH1Fs *h_Lepton_Eta;
   TH1Fs *h_Lepton_Pt;
   TH1Fs *h_nJet;
   TH1Fs *h_Mwt;
   TH1Fs *h_MinDeltaPhiJETMET;
   TH1Fs *h_HT;
   TH1Fs *h_HT_all;

   TH1Fs *h_HT_5j;
   TH1Fs *h_HT_all_5j;
   TH1Fs *h_HT_6j;
   TH1Fs *h_HT_all_6j;
   TH1Fs *h_HT_7j;
   TH1Fs *h_HT_all_7j;
   TH1Fs *h_HT_8j;
   TH1Fs *h_HT_all_8j;
   TH1Fs *h_HT_9j;
   TH1Fs *h_HT_all_9j;

   TH1Fs *h_HT_all_ntup_1b;
   TH1Fs *h_HT_all_ntup_1c;
   TH1Fs *h_HT_all_ntup_1l;
   TH1Fs *h_HT_all_ntup;

   TH1Fs *h_HT_bjets;
   TH1Fs *h_mVH;
   TH1Fs *h_DeltaPhi_HW;
   TH1Fs *h_DeltaEta_HW;
   TH1Fs *h_maxMVAResponse;
   TH1Fs *h_pTH;
   TH1Fs *h_pTH_over_mVH;
   TH1Fs *h_pTWplus;
   TH1Fs *h_pTW_over_mVH;
   TH1Fs *h_pTWminus;
   TH1Fs *h_etaWminus;
   TH1Fs *h_massWminus;
   TH1Fs *h_phiWminus;
   TH1Fs *h_mH;
   TH1Fs *h_mWplus;
   TH1Fs *h_mWinus;
   TH1Fs *h_tagCategory;
   TH1Fs *h_mass_resolution;
   TH1Fs *h_top_mass_resolution;
   TH1Fs *h_had_t;
   TH1Fs *h_evw;
   TH1Fs *h_evw_trueM_bdt;
   TH1Fs *h_evw_trueM_eone;
   TH1Fs *h_evw_trueM_eone_MVA07;
   TH1Fs *h_evw_trueM_eone_MVA08;
   TH1Fs *h_evw_trueM_eone_MVA09;
   TH1Fs *h_evw_trueM_eone_MVA095;
   TH1Fs *h_evw_trueM_eone_lvbb;
   TH1Fs *h_evw_trueM_etwo;
   TH1Fs *h_evw_trueM_ethree;
   TH1Fs *h_evw_trueM_gefour;
   TH1Fs *h_evw_j1j2;
   TH1Fs *h_evw_j3j4;
   TH1Fs *h_evw_MVA;
   TH1Fs *h_evw_MVAaccu;

   TH1Fs *h_evw_MVAaccu_MVA07;
   TH1Fs *h_evw_MVAaccu_MVA08;
   TH1Fs *h_evw_MVAaccu_MVA09;
   TH1Fs *h_evw_MVAaccu_MVA095;

   TH1Fs *h_evw_MVAaccu2;
   TH1Fs *h_evw_MVAaccu4;
   TH1Fs *h_evw_MVAaccu5;
   TH1Fs *h_evw_MVAaccu6;
   TH1Fs *h_evw_Whad;
   TH1Fs *h_evw_Whad_MVA07;
   TH1Fs *h_evw_Whad_MVA08;
   TH1Fs *h_evw_Whad_MVA09;
   TH1Fs *h_evw_Whad_MVA095;
   TH1Fs *h_evw_Wlep;
   TH1Fs *h_evw_qqbbrec;
   TH1Fs *h_lep_top_mass;
   TH1Fs *h_lep_top_pT;
   TH1Fs *h_lep_top_mass_minpT;
   TH1Fs *h_lep_top_pT_minpT;
   TH1Fs *h_lep_top_mass_minXwT;
   TH1Fs *h_lep_top_pT_minXwT;
   TH1Fs *h_abstop_res;
   
   TH1Fs *h_mVH_true;
   //TH1Fs *h_mass_resolution;
   TH1Fs *h_mass_resolution_MVA07;
   TH1Fs *h_mass_resolution_MVA08;
   TH1Fs *h_mass_resolution_MVA09;
   TH1Fs *h_mass_resolution_MVA095;

   TH1Fs *h_mass_resolution_200;
   TH1Fs *h_mass_resolution_225;
   TH1Fs *h_mass_resolution_250;
   TH1Fs *h_mass_resolution_275;

   TH1Fs *h_NBtags_2sig;
   TH1Fs *h_MET_2sig;
   //TH1Fs *h_METSig_2sig;
   TH1Fs *h_Lepton_Eta_2sig;
   TH1Fs *h_Lepton_Pt_2sig;
   TH1Fs *h_nJet_2sig;
   TH1Fs *h_Mwt_2sig;
   TH1Fs *h_MinDeltaPhiJETMET_2sig;
   TH1Fs *h_HT_2sig;
   TH1Fs *h_HT_bjets_2sig;
   TH1Fs *h_mVH_2sig;
   TH1Fs *h_DeltaPhi_HW_2sig;
   TH1Fs *h_DeltaEta_HW_2sig;
   TH1Fs *h_maxMVAResponse_2sig;
   TH1Fs *h_pTH_2sig;
   TH1Fs *h_pTH_over_mVH_2sig;
   TH1Fs *h_pTWplus_2sig;
   TH1Fs *h_pTW_over_mVH_2sig;
   TH1Fs *h_pTWminus_2sig;
   TH1Fs *h_mH_2sig;
   TH1Fs *h_mWplus_2sig;
   TH1Fs *h_tagCategory_2sig;
   TH1Fs *h_mass_resolution_2sig;

   TH1Fs *h_NBtags_1b;
   TH1Fs *h_MET_1b;
   TH1Fs *h_Lepton_Eta_1b;
   TH1Fs *h_Lepton_Pt_1b;
   TH1Fs *h_nJet_1b;
   TH1Fs *h_Mwt_1b;
   TH1Fs *h_MinDeltaPhiJETMET_1b;
   TH1Fs *h_HT_1b;
   TH1Fs *h_HT_all_1b;

   TH1Fs *h_HT_5j_1b;
   TH1Fs *h_HT_all_5j_1b;
   TH1Fs *h_HT_6j_1b;
   TH1Fs *h_HT_all_6j_1b;
   TH1Fs *h_HT_7j_1b;
   TH1Fs *h_HT_all_7j_1b;
   TH1Fs *h_HT_8j_1b;
   TH1Fs *h_HT_all_8j_1b;
   TH1Fs *h_HT_9j_1b;
   TH1Fs *h_HT_all_9j_1b;

   TH1Fs *h_HT_bjets_1b;
   TH1Fs *h_mVH_1b;
   TH1Fs *h_DeltaPhi_HW_1b;
   TH1Fs *h_DeltaEta_HW_1b;
   TH1Fs *h_maxMVAResponse_1b;
   TH1Fs *h_pTH_1b;
   TH1Fs *h_pTH_over_mVH_1b;
   TH1Fs *h_pTWplus_1b;
   TH1Fs *h_pTW_over_mVH_1b;
   TH1Fs *h_pTWminus_1b;
   TH1Fs *h_etaWminus_1b;
   TH1Fs *h_massWminus_1b;
   TH1Fs *h_phiWminus_1b;
   TH1Fs *h_mH_1b;
   TH1Fs *h_mWplus_1b;
   TH1Fs *h_tagCategory_1b;
   TH1Fs *h_mass_resolution_1b; 
   TH1Fs *h_lep_top_mass_1b;
   TH1Fs *h_lep_top_pT_1b;
   TH1Fs *h_lep_top_mass_minpT_1b;
   TH1Fs *h_lep_top_pT_minpT_1b;
   TH1Fs *h_lep_top_mass_minXwT_1b;
   TH1Fs *h_lep_top_pT_minXwT_1b;
   
   TH1Fs *h_pTWplus_5j_1b;
   TH1Fs *h_pTH_5j_1b;
   TH1Fs *h_maxMVAResponse_5j_1b;
   TH1Fs *h_DeltaPhi_HW_5j_1b;
   TH1Fs *h_DeltaEta_HW_5j_1b;
   TH1Fs *h_Mwt_5j_1b;
   TH1Fs *h_mVH_5j_1b;

   TH1Fs *h_pTWplus_5j_1c;
   TH1Fs *h_pTH_5j_1c;
   TH1Fs *h_maxMVAResponse_5j_1c;
   TH1Fs *h_DeltaPhi_HW_5j_1c;
   TH1Fs *h_DeltaEta_HW_5j_1c;
   TH1Fs *h_Mwt_5j_1c;
   TH1Fs *h_mVH_5j_1c;

   TH1Fs *h_pTWplus_5j_1l;
   TH1Fs *h_pTH_5j_1l;
   TH1Fs *h_maxMVAResponse_5j_1l;
   TH1Fs *h_DeltaPhi_HW_5j_1l;
   TH1Fs *h_DeltaEta_HW_5j_1l;
   TH1Fs *h_Mwt_5j_1l;
   TH1Fs *h_mVH_5j_1l;

   TH1Fs *h_pTWplus_6j_1b;
   TH1Fs *h_pTH_6j_1b;
   TH1Fs *h_maxMVAResponse_6j_1b;
   TH1Fs *h_DeltaPhi_HW_6j_1b;
   TH1Fs *h_DeltaEta_HW_6j_1b;
   TH1Fs *h_Mwt_6j_1b;
   TH1Fs *h_mVH_6j_1b;

   TH1Fs *h_pTWplus_6j_1c;
   TH1Fs *h_pTH_6j_1c;
   TH1Fs *h_maxMVAResponse_6j_1c;
   TH1Fs *h_DeltaPhi_HW_6j_1c;
   TH1Fs *h_DeltaEta_HW_6j_1c;
   TH1Fs *h_Mwt_6j_1c;
   TH1Fs *h_mVH_6j_1c;

   TH1Fs *h_pTWplus_6j_1l;
   TH1Fs *h_pTH_6j_1l;
   TH1Fs *h_maxMVAResponse_6j_1l;
   TH1Fs *h_DeltaPhi_HW_6j_1l;
   TH1Fs *h_DeltaEta_HW_6j_1l;
   TH1Fs *h_Mwt_6j_1l;
   TH1Fs *h_mVH_6j_1l;
   
   TH1Fs *h_pTWplus_7j_1b;
   TH1Fs *h_pTH_7j_1b;
   TH1Fs *h_maxMVAResponse_7j_1b;
   TH1Fs *h_DeltaPhi_HW_7j_1b;
   TH1Fs *h_DeltaEta_HW_7j_1b;
   TH1Fs *h_Mwt_7j_1b;
   TH1Fs *h_mVH_7j_1b;

   TH1Fs *h_pTWplus_7j_1c;
   TH1Fs *h_pTH_7j_1c;
   TH1Fs *h_maxMVAResponse_7j_1c;
   TH1Fs *h_DeltaPhi_HW_7j_1c;
   TH1Fs *h_DeltaEta_HW_7j_1c;
   TH1Fs *h_Mwt_7j_1c;
   TH1Fs *h_mVH_7j_1c;

   TH1Fs *h_pTWplus_7j_1l;
   TH1Fs *h_pTH_7j_1l;
   TH1Fs *h_maxMVAResponse_7j_1l;
   TH1Fs *h_DeltaPhi_HW_7j_1l;
   TH1Fs *h_DeltaEta_HW_7j_1l;
   TH1Fs *h_Mwt_7j_1l;
   TH1Fs *h_mVH_7j_1l;

   TH1Fs *h_pTWplus_8j_1b;
   TH1Fs *h_pTH_8j_1b;
   TH1Fs *h_maxMVAResponse_8j_1b;
   TH1Fs *h_DeltaPhi_HW_8j_1b;
   TH1Fs *h_DeltaEta_HW_8j_1b;
   TH1Fs *h_Mwt_8j_1b;
   TH1Fs *h_mVH_8j_1b;

   TH1Fs *h_pTWplus_8j_1c;
   TH1Fs *h_pTH_8j_1c;
   TH1Fs *h_maxMVAResponse_8j_1c;
   TH1Fs *h_DeltaPhi_HW_8j_1c;
   TH1Fs *h_DeltaEta_HW_8j_1c;
   TH1Fs *h_Mwt_8j_1c;
   TH1Fs *h_mVH_8j_1c;

   TH1Fs *h_pTWplus_8j_1l;
   TH1Fs *h_pTH_8j_1l;
   TH1Fs *h_maxMVAResponse_8j_1l;
   TH1Fs *h_DeltaPhi_HW_8j_1l;
   TH1Fs *h_DeltaEta_HW_8j_1l;
   TH1Fs *h_Mwt_8j_1l;
   TH1Fs *h_mVH_8j_1l;

   TH1Fs *h_pTWplus_9j_1b;
   TH1Fs *h_pTH_9j_1b;
   TH1Fs *h_maxMVAResponse_9j_1b;
   TH1Fs *h_DeltaPhi_HW_9j_1b;
   TH1Fs *h_DeltaEta_HW_9j_1b;
   TH1Fs *h_Mwt_9j_1b;
   TH1Fs *h_mVH_9j_1b;

   TH1Fs *h_pTWplus_9j_1c;
   TH1Fs *h_pTH_9j_1c;
   TH1Fs *h_maxMVAResponse_9j_1c;
   TH1Fs *h_DeltaPhi_HW_9j_1c;
   TH1Fs *h_DeltaEta_HW_9j_1c;
   TH1Fs *h_Mwt_9j_1c;
   TH1Fs *h_mVH_9j_1c;

   TH1Fs *h_pTWplus_9j_1l;
   TH1Fs *h_pTH_9j_1l;
   TH1Fs *h_maxMVAResponse_9j_1l;
   TH1Fs *h_DeltaPhi_HW_9j_1l;
   TH1Fs *h_DeltaEta_HW_9j_1l;
   TH1Fs *h_Mwt_9j_1l;
   TH1Fs *h_mVH_9j_1l;

   TH1Fs *h_pTWplus_5j;
   TH1Fs *h_pTH_5j;
   TH1Fs *h_maxMVAResponse_5j;
   TH1Fs *h_DeltaPhi_HW_5j;
   TH1Fs *h_DeltaEta_HW_5j;
   TH1Fs *h_Mwt_5j;
   TH1Fs *h_mVH_5j;

   TH1Fs *h_pTWplus_6j;
   TH1Fs *h_pTH_6j;
   TH1Fs *h_maxMVAResponse_6j;
   TH1Fs *h_DeltaPhi_HW_6j;
   TH1Fs *h_DeltaEta_HW_6j;
   TH1Fs *h_Mwt_6j;
   TH1Fs *h_mVH_6j;

   TH1Fs *h_pTWplus_7j;
   TH1Fs *h_pTH_7j;
   TH1Fs *h_maxMVAResponse_7j;
   TH1Fs *h_DeltaPhi_HW_7j;
   TH1Fs *h_DeltaEta_HW_7j;
   TH1Fs *h_Mwt_7j;
   TH1Fs *h_mVH_7j;

   TH1Fs *h_pTWplus_8j;
   TH1Fs *h_pTH_8j;
   TH1Fs *h_maxMVAResponse_8j;
   TH1Fs *h_DeltaPhi_HW_8j;
   TH1Fs *h_DeltaEta_HW_8j;
   TH1Fs *h_Mwt_8j;
   TH1Fs *h_mVH_8j;

   //TH1Fs *h_mVH_5j;
   //TH1Fs *h_mVH_6j;
   //TH1Fs *h_mVH_7j;
   //TH1Fs *h_mVH_8j;
   //TH1Fs *h_mVH_9j;

   TH1Fs *h_pTWplus_9j;
   TH1Fs *h_pTH_9j;
   TH1Fs *h_maxMVAResponse_9j;
   TH1Fs *h_DeltaPhi_HW_9j;
   TH1Fs *h_DeltaEta_HW_9j;
   TH1Fs *h_Mwt_9j;
   TH1Fs *h_mVH_9j;

   TH1Fs *h_lepch;
   
   TH1Fs *h_NBtags_1c;
   TH1Fs *h_MET_1c;
   TH1Fs *h_Lepton_Eta_1c;
   TH1Fs *h_Lepton_Pt_1c;
   TH1Fs *h_nJet_1c;
   TH1Fs *h_Mwt_1c;
   TH1Fs *h_MinDeltaPhiJETMET_1c;
   TH1Fs *h_HT_1c;
   TH1Fs *h_HT_all_1c;

   TH1Fs *h_HT_5j_1c;
   TH1Fs *h_HT_all_5j_1c;
   TH1Fs *h_HT_6j_1c;
   TH1Fs *h_HT_all_6j_1c;
   TH1Fs *h_HT_7j_1c;
   TH1Fs *h_HT_all_7j_1c;
   TH1Fs *h_HT_8j_1c;
   TH1Fs *h_HT_all_8j_1c;
   TH1Fs *h_HT_9j_1c;
   TH1Fs *h_HT_all_9j_1c;

   TH1Fs *h_HT_bjets_1c;
   TH1Fs *h_mVH_1c;
   TH1Fs *h_DeltaPhi_HW_1c;
   TH1Fs *h_DeltaEta_HW_1c;
   TH1Fs *h_maxMVAResponse_1c;
   TH1Fs *h_pTH_1c;
   TH1Fs *h_pTH_over_mVH_1c;
   TH1Fs *h_pTWplus_1c;
   TH1Fs *h_pTW_over_mVH_1c;
   TH1Fs *h_pTWminus_1c;
   TH1Fs *h_etaWminus_1c;
   TH1Fs *h_massWminus_1c;
   TH1Fs *h_phiWminus_1c;
   TH1Fs *h_mH_1c;
   TH1Fs *h_mWplus_1c;
   TH1Fs *h_tagCategory_1c;
   TH1Fs *h_mass_resolution_1c;
   TH1Fs *h_lep_top_mass_1c;
   TH1Fs *h_lep_top_pT_1c;
   TH1Fs *h_lep_top_mass_minpT_1c;
   TH1Fs *h_lep_top_pT_minpT_1c;
   TH1Fs *h_lep_top_mass_minXwT_1c;
   TH1Fs *h_lep_top_pT_minXwT_1c;

   TH1Fs *h_NBtags_1l;
   TH1Fs *h_MET_1l;
   TH1Fs *h_Lepton_Eta_1l;
   TH1Fs *h_Lepton_Pt_1l;
   TH1Fs *h_nJet_1l;
   TH1Fs *h_Mwt_1l;
   TH1Fs *h_MinDeltaPhiJETMET_1l;
   TH1Fs *h_HT_1l;
   TH1Fs *h_HT_all_1l;

   TH1Fs *h_HT_5j_1l;
   TH1Fs *h_HT_all_5j_1l;
   TH1Fs *h_HT_6j_1l;
   TH1Fs *h_HT_all_6j_1l;
   TH1Fs *h_HT_7j_1l;
   TH1Fs *h_HT_all_7j_1l;
   TH1Fs *h_HT_8j_1l;
   TH1Fs *h_HT_all_8j_1l;
   TH1Fs *h_HT_9j_1l;
   TH1Fs *h_HT_all_9j_1l;

   TH1Fs *h_HT_bjets_1l;
   TH1Fs *h_mVH_1l;
   TH1Fs *h_DeltaPhi_HW_1l;
   TH1Fs *h_DeltaEta_HW_1l;
   TH1Fs *h_maxMVAResponse_1l;
   TH1Fs *h_pTH_1l;
   TH1Fs *h_pTH_over_mVH_1l;
   TH1Fs *h_pTWplus_1l;
   TH1Fs *h_pTW_over_mVH_1l;
   TH1Fs *h_pTWminus_1l;
   TH1Fs *h_etaWminus_1l;
   TH1Fs *h_massWminus_1l;
   TH1Fs *h_phiWminus_1l;
   TH1Fs *h_mH_1l;
   TH1Fs *h_mWplus_1l;
   TH1Fs *h_tagCategory_1l;
   TH1Fs *h_mass_resolution_1l;
   TH1Fs *h_lep_top_mass_1l;
   TH1Fs *h_lep_top_pT_1l;
   TH1Fs *h_lep_top_mass_minpT_1l;
   TH1Fs *h_lep_top_pT_minpT_1l;
   TH1Fs *h_lep_top_mass_minXwT_1l;
   TH1Fs *h_lep_top_pT_minXwT_1l;
   
   //reweighted histogram definition
   TH1Fs *h_nJet_1b_rew;
   TH1Fs *h_HT_1b_rew;
   TH1Fs *h_mVH_1b_rew;
   //TH1Fs *h_DeltaPhi_HW_1b_rew;
   //TH1Fs *h_DeltaEta_HW_1b_rew;
   TH1Fs *h_maxMVAResponse_1b_rew;
   TH1Fs *h_pTH_1b_rew;
   TH1Fs *h_pTH_over_mVH_1b_rew;
   TH1Fs *h_pTWplus_1b_rew;
   TH1Fs *h_pTW_over_mVH_1b_rew;

   TH1Fs *h_nJet_1l_rew;
   TH1Fs *h_HT_1l_rew;
   TH1Fs *h_mVH_1l_rew;
   //TH1Fs *h_DeltaPhi_HW_1l_rew;
   //TH1Fs *h_DeltaEta_HW_1l_rew;
   TH1Fs *h_maxMVAResponse_1l_rew;
   TH1Fs *h_pTH_1l_rew;
   TH1Fs *h_pTH_over_mVH_1l_rew;
   TH1Fs *h_pTWplus_1l_rew;
   TH1Fs *h_pTW_over_mVH_1l_rew;
   
   TH1Fs *h_pTH_250Res_1b;
   TH1Fs *h_pTH_400Res_1b;
   TH1Fs *h_pTH_800Res_1b;
   TH1Fs *h_pTH_1200Res_1b;
   TH1Fs *h_pTH_1600Res_1b;
   TH1Fs *h_pTH_2000Res_1b;
   
   TH1Fs *h_pTH_250Res_1c;
   TH1Fs *h_pTH_400Res_1c;
   TH1Fs *h_pTH_800Res_1c;
   TH1Fs *h_pTH_1200Res_1c;
   TH1Fs *h_pTH_1600Res_1c;
   TH1Fs *h_pTH_2000Res_1c;
   
   TH1Fs *h_pTH_250Res_1l;
   TH1Fs *h_pTH_400Res_1l;
   TH1Fs *h_pTH_800Res_1l;
   TH1Fs *h_pTH_1200Res_1l;
   TH1Fs *h_pTH_1600Res_1l;
   TH1Fs *h_pTH_2000Res_1l;
   
   TH1Fs *h_pTWplus_250Res_1b;
   TH1Fs *h_pTWplus_400Res_1b;
   TH1Fs *h_pTWplus_800Res_1b;
   TH1Fs *h_pTWplus_1200Res_1b;
   TH1Fs *h_pTWplus_1600Res_1b;
   TH1Fs *h_pTWplus_2000Res_1b;
   
   TH1Fs *h_pTWplus_250Res_1c;
   TH1Fs *h_pTWplus_400Res_1c;
   TH1Fs *h_pTWplus_800Res_1c;
   TH1Fs *h_pTWplus_1200Res_1c;
   TH1Fs *h_pTWplus_1600Res_1c;
   TH1Fs *h_pTWplus_2000Res_1c;
   
   TH1Fs *h_pTWplus_250Res_1l;
   TH1Fs *h_pTWplus_400Res_1l;
   TH1Fs *h_pTWplus_800Res_1l;
   TH1Fs *h_pTWplus_1200Res_1l;
   TH1Fs *h_pTWplus_1600Res_1l;
   TH1Fs *h_pTWplus_2000Res_1l;
   
   TH1Fs *h_maxMVAResponse_250Res_1b;
   TH1Fs *h_maxMVAResponse_400Res_1b;
   TH1Fs *h_maxMVAResponse_800Res_1b;
   TH1Fs *h_maxMVAResponse_1200Res_1b;
   TH1Fs *h_maxMVAResponse_1600Res_1b;
   TH1Fs *h_maxMVAResponse_2000Res_1b;
   
   TH1Fs *h_maxMVAResponse_250Res_1c;
   TH1Fs *h_maxMVAResponse_400Res_1c;
   TH1Fs *h_maxMVAResponse_800Res_1c;
   TH1Fs *h_maxMVAResponse_1200Res_1c;
   TH1Fs *h_maxMVAResponse_1600Res_1c;
   TH1Fs *h_maxMVAResponse_2000Res_1c;
   
   TH1Fs *h_maxMVAResponse_250Res_1l;
   TH1Fs *h_maxMVAResponse_400Res_1l;
   TH1Fs *h_maxMVAResponse_800Res_1l;
   TH1Fs *h_maxMVAResponse_1200Res_1l;
   TH1Fs *h_maxMVAResponse_1600Res_1l;
   TH1Fs *h_maxMVAResponse_2000Res_1l;

   TH1Fs *h_pTH_250Res;
   TH1Fs *h_pTH_400Res;
   TH1Fs *h_pTH_800Res;
   TH1Fs *h_pTH_1200Res;
   TH1Fs *h_pTH_1600Res;
   TH1Fs *h_pTH_2000Res;
   
   TH1Fs *h_pTWplus_250Res;
   TH1Fs *h_pTWplus_400Res;
   TH1Fs *h_pTWplus_800Res;
   TH1Fs *h_pTWplus_1200Res;
   TH1Fs *h_pTWplus_1600Res;
   TH1Fs *h_pTWplus_2000Res;
   
   TH1Fs *h_maxMVAResponse_250Res;
   TH1Fs *h_maxMVAResponse_400Res;
   TH1Fs *h_maxMVAResponse_800Res;
   TH1Fs *h_maxMVAResponse_1200Res;
   TH1Fs *h_maxMVAResponse_1600Res;
   TH1Fs *h_maxMVAResponse_2000Res;
   
   TH1Fs *h_dRq1j1;
   TH1Fs *h_dRq2j2;
   TH1Fs *h_dRb1j1;
   TH1Fs *h_dRb2j2;
   
   TH1Fs *h_dRHjj;
   TH1Fs *h_dRWjj;

   TH1Fs *h_evw_qqbb;
   TH1Fs *h_evw_lvbb;
   TH1Fs *h_evw_lecqqbb;
   TH1Fs *h_evw_leclvbb;
   TH1Fs *h_evw_Genqqbb;
   TH1Fs *h_evw_Genlvbb;
   TH1Fs *h_evw_neglep;
   TH1Fs *h_evw_poslep;
   //TH1Fs *h_pTWplus_1c_rew;

   TH1Fs *h_nJet_1c_rew;
   TH1Fs *h_HT_1c_rew;
   TH1Fs *h_mVH_1c_rew;
   //TH1Fs *h_DeltaPhi_HW_1c_rew;
   //TH1Fs *h_DeltaEta_HW_1c_rew;
   TH1Fs *h_maxMVAResponse_1c_rew;
   TH1Fs *h_pTH_1c_rew;
   TH1Fs *h_pTH_over_mVH_1c_rew;
   TH1Fs *h_pTWplus_1c_rew;
   TH1Fs *h_pTW_over_mVH_1c_rew;

   TH1Fs *h_Wlep_pT;
   TH1Fs *h_Whad_pT;
   TH1Fs *h_Wlep_eta;
   TH1Fs *h_Whad_eta;
   TH1Fs *h_Wlep_phi;
   TH1Fs *h_Whad_phi;
   TH1Fs *h_Higgs_pT;
   TH1Fs *h_Higgs_eta;
   TH1Fs *h_Higgs_phi;

   TH1Fs *h_mVH_09;
   TH1Fs *h_mVH_29;
   TH1Fs *h_mVH_49;
   TH1Fs *h_mVH_69;
   TH1Fs *h_mVH_07;
   TH1Fs *h_mVH_27;
   TH1Fs *h_mVH_47;
   TH1Fs *h_mVH_67;
   TH1Fs *h_mVH_10;
   TH1Fs *h_mVH_50;
   TH1Fs *h_mVH_52;
   TH1Fs *h_mVH_54;
   TH1Fs *h_mVH_56;
   TH1Fs *h_mVH_19;
   TH1Fs *h_mVH_7;
   TH1Fs *h_mVH_8;
   TH1Fs *h_mVH_9;
   TH1Fs *h_mVH_95;

   TH1Fs *h_mVH_09_1c;
   TH1Fs *h_mVH_29_1c;
   TH1Fs *h_mVH_49_1c;
   TH1Fs *h_mVH_69_1c;
   TH1Fs *h_mVH_07_1c;
   TH1Fs *h_mVH_27_1c;
   TH1Fs *h_mVH_47_1c;
   TH1Fs *h_mVH_67_1c;
   TH1Fs *h_mVH_10_1c;
   TH1Fs *h_mVH_50_1c;
   TH1Fs *h_mVH_52_1c;
   TH1Fs *h_mVH_54_1c;
   TH1Fs *h_mVH_56_1c;
   TH1Fs *h_mVH_19_1c;
   TH1Fs *h_mVH_7_1c;
   TH1Fs *h_mVH_8_1c;
   TH1Fs *h_mVH_9_1c;
   TH1Fs *h_mVH_95_1c;

   TH1Fs *h_mVH_09_1b;
   TH1Fs *h_mVH_29_1b;
   TH1Fs *h_mVH_49_1b;
   TH1Fs *h_mVH_69_1b;
   TH1Fs *h_mVH_07_1b;
   TH1Fs *h_mVH_27_1b;
   TH1Fs *h_mVH_47_1b;
   TH1Fs *h_mVH_67_1b;
   TH1Fs *h_mVH_10_1b;
   TH1Fs *h_mVH_50_1b;
   TH1Fs *h_mVH_52_1b;
   TH1Fs *h_mVH_54_1b;
   TH1Fs *h_mVH_56_1b;
   TH1Fs *h_mVH_19_1b;
   TH1Fs *h_mVH_7_1b;
   TH1Fs *h_mVH_8_1b;
   TH1Fs *h_mVH_9_1b;
   TH1Fs *h_mVH_95_1b;

   TH1Fs *h_mVH_09_1l;
   TH1Fs *h_mVH_29_1l;
   TH1Fs *h_mVH_49_1l;
   TH1Fs *h_mVH_69_1l;
   TH1Fs *h_mVH_07_1l;
   TH1Fs *h_mVH_27_1l;
   TH1Fs *h_mVH_47_1l;
   TH1Fs *h_mVH_67_1l;
   TH1Fs *h_mVH_10_1l;
   TH1Fs *h_mVH_50_1l;
   TH1Fs *h_mVH_52_1l;
   TH1Fs *h_mVH_54_1l;
   TH1Fs *h_mVH_56_1l;
   TH1Fs *h_mVH_19_1l;
   TH1Fs *h_mVH_7_1l;
   TH1Fs *h_mVH_8_1l;
   TH1Fs *h_mVH_9_1l;
   TH1Fs *h_mVH_95_1l;

   //reweighted histogram definition

   vector<double>  m_EventWeights_wott;
   vector<double> m_EventWeights;
   vector<double>  m_ones;
   vector<TString> m_UncNames;
   vector<float> m_LepCh;
   vector<float> m_btagSF_CUP;
   vector<float> m_btagSF_CDOWN;
   vector<float> m_btagSF_BUP;
   vector<float> m_btagSF_BDOWN;
   vector<float> m_btagSF_LUP;
   vector<float> m_btagSF_LDOWN;
   vector<TString> mySel;

   std::vector<TLorentzVector> bQuarks;
   std::vector<TLorentzVector> LightQuarks;   
   std::vector<TLorentzVector> Leptons;
   std::vector<TLorentzVector> GenLevLeptons;
   std::vector<TLorentzVector> GenLevLeptons_I;
   std::vector<TLorentzVector> GenLevLeptons_II;
   std::vector<TLorentzVector> Jets;
   std::vector<TLorentzVector> FJets;
   std::vector<TLorentzVector> TrkJets;
   std::vector<TLorentzVector> TruthbJets;
   std::vector<TLorentzVector> TruthlightJets;
   std::vector<TLorentzVector> TruthbJets_MatchPar;
   std::vector<TLorentzVector> TruthlightJets_MatchPar;
   std::vector<int> Jets_PCbtag;
   //std::vector<int> TrkJets_PCbtag;
   //std::vector<int> nTaggedVRTrkJetsInFJet;

   TLorentzVector Lepton4vector;
   TLorentzVector Lep_top;
   TLorentzVector Lep_top_minpT;
   TLorentzVector Lep_top_minXwT;
   TLorentzVector Lep_top_min;
   TLorentzVector Lep_top_minX;
   TLorentzVector Had_top;
   TLorentzVector MET;
   TLorentzVector Higgs;
   TLorentzVector Wplus;
   TLorentzVector Higgs_tr;
   TLorentzVector Wplus_tr;
   TLorentzVector Higgs_pole;
   TLorentzVector Wplus_had_pole;
   TLorentzVector Wplus_lep_pole;
   TLorentzVector Wminus;
   TLorentzVector Higgs_LV;
   TLorentzVector Wplus_LV;
   TLorentzVector Higgs_p1;
   TLorentzVector Higgs_p2;
   TLorentzVector Wplus_p1;
   TLorentzVector Wplus_p2;
   TLorentzVector LepTop;
   TLorentzVector HadTop;
   bool   found_Wplus_had;
   bool   found_Wplus_lep;
   bool   found_Higgs;
   bool   found_LepTop;
   bool   found_HadTop;
   bool   m_countdRj1j2;
   bool   m_countdRj3j4;
   int    m_is_Signal;
   float  m_costhetastar;
   Long_t   m_eventnum;
   TLorentzVector sel_jet;

   int    m_NTags_S;
   int    m_NTags;

   int    m_NTags_PreSel;

   int    m_NTags_caloJ;
   int    m_NTags_trkJ;
   int    m_NTags_Higgs;
   int    m_ntagsOutside;
   int    m_bTagCategory;
   int    m_btagCategoryBin;
   int    m_index_H1;
   int    m_index_H2;
   int    m_index_W1;
   int    m_index_W2;
   int    m_index_bj;
   int    m_true_count;
   int    m_true_count_lvbb;
   int    m_accu_count;
   int    m_accu_count2;
   int    m_accu_count4;
   int    m_accu_count5;
   int    m_accu_count6;
   float m_min_dRTruth;
   float m_min_chi2;
   float m_min_DeltaPhiJETMET;
   float m_MaxMVA_Response;
   float m_HT;
   float m_HT_all;
   float m_HT_bjets;
   float m_maxEta_bjets;
   float m_maxPT_bjets;
   float m_mWT;
   float m_Lep_pT;
   float m_Lep_Eta;
   float m_mVH;
   float m_DeltaPhi_HW;
   float m_DeltaEta_HW;
   float m_Wleptonic_pT;
   float m_Wleptonic_Eta;
   float m_MassTruth;
   float m_MET;
   float m_HT6j;
   float m_dR_lj_min;
   float m_dR_lj_max;
   float m_bTagScoreSum4j;
   float m_bTagScoreSum6j;
   float m_H_mass;    
   float m_H_pT;
   float m_pTjH1;
   float m_pTjH2;
   float m_btagjH1;
   float m_btagjH2;
   float m_dRjjH;
   float m_Wp_mass;
   float m_Wp_pT;
   float m_pTjWp1;
   float m_pTjWp2;
   float m_btagjWp1;
   float m_btagjWp2;
   float m_dRjjWp;
   float m_Phi_HW;
   float m_Eta_HW;
   float m_dR_HW;
   float m_mass_VH;
   float m_pTH_over_mvH;

   float m_maxMVA;
   float m_pTHomVH;
   float m_pTWomVH;
   float m_btagH1;
   float m_btagH2;
   float m_btagW1;
   float m_btagW2;
   float m_H;
   float m_W;

   float m_ptW_over_mvH;
   float m_weight_scale;
   int m_flag_tt;
   float m_weight_norm;
   float m_scale;
   int m_topcon;

   float massWH;
   int  nJets;
   float maxMVA;
   float pTWplus;
   float pTHiggs;

   float m_min_dRq1j1;
   float m_min_dRq2j2;
   float m_min_dRb1j1;
   float m_min_dRb2j2;

   float m_min_dRHjj;
   float m_min_dRWjj;

   float pTlep;
   float etalep;
   float DelEtaHW;
   float DelPhiHW;
   float HT_bjet;
   float pTHiggs_over_massWH;
   float pTWplus_over_massWH;
   float massHiggs;
   float massWplus;
   float btagj1H;
   float btagj2H; 
   float btagj1W;
   float btagj2W;
   float leptop_mass;
   float pTj1H;
   float pTj2H;
   float pTj1W;
   float pTj2W;
   float etaj1H;
   float etaj2H;
   float etaj1W;
   float etaj2W;

   int nBTags;
   int HFClass;
   int mcChanNbr;
   int TopHeavyFF;
   float weight_btagSF;
   float weight_normal;
   float weight_MC;
   float weight_jetvt;
   float weight_lepSF;
   float Mwt;
   float HT;
   int ttFlag;
   
   //muon weight 
   float weight_lepSF_mu_TTVA_Syst_UP;
   float weight_lepSF_mu_TTVA_Syst_DOWN;
   float weight_lepSF_mu_TTVA_Stat_UP;
   float weight_lepSF_mu_TTVA_Stat_DOWN;

   float weight_lepSF_mu_Trigger_Syst_UP;
   float weight_lepSF_mu_Trigger_Syst_DOWN;
   float weight_lepSF_mu_Trigger_Stat_UP;
   float weight_lepSF_mu_Trigger_Stat_DOWN;

   float weight_lepSF_mu_ID_Syst_UP;
   float weight_lepSF_mu_ID_Syst_DOWN;
   float weight_lepSF_mu_ID_Stat_UP;
   float weight_lepSF_mu_ID_Stat_DOWN;

   float weight_lepSF_mu_Isol_Syst_UP;
   float weight_lepSF_mu_Isol_Syst_DOWN;
   float weight_lepSF_mu_Isol_Stat_UP;
   float weight_lepSF_mu_Isol_Stat_DOWN;

   float weight_lepSF_mu_ID_lowpt_Syst_UP;
   float weight_lepSF_mu_ID_lowpt_Syst_DOWN;
   float weight_lepSF_mu_ID_lowpt_Stat_UP;
   float weight_lepSF_mu_ID_lowpt_Stat_DOWN;

   //electron weight
   float weight_lepSF_el_Isol_UP;
   float weight_lepSF_el_Isol_DOWN;
   
   float weight_lepSF_el_ID_UP;
   float weight_lepSF_el_ID_DOWN;

   float weight_lepSF_el_Reco_UP;
   float weight_lepSF_el_Reco_DOWN;

   float weight_lepSF_el_Trigger_UP;
   float weight_lepSF_el_Trigger_DOWN;

  
   float weight_pu;
   float weight_pu_UP;
   float weight_pu_DOWN;
   float weight_jetvt_UP;
   float weight_jetvt_DOWN;
   vector<float> weight_btagSF_CUP;
   vector<float> weight_btagSF_CDOWN;
   vector<float> weight_btagSF_BUP;
   vector<float> weight_btagSF_BDOWN;
   vector<float> weight_btagSF_LUP;
   vector<float> weight_btagSF_LDOWN;
   float weight_normal_mu2f2;
   float weight_normal_mu5f5;
   float weight_normal_fsrUP;
   float weight_normal_fsrDOWN;
   float weight_normal_varUP;
   float weight_normal_varDOWN;
   float weight_mu2f2;
   float weight_mu5f5;
   float weight_fsrUP;
   float weight_fsrDOWN;
   float weight_varUP;
   float weight_varDOWN;
   //bool JetPairqqbb;
   vector<double>  EvtWeight_wott;
   vector<double>  EvtWeight_wtt;
   //float MC16_scale;
};

#endif

#ifdef EventLoop_cxx
EventLoop::EventLoop(TTree *tree, TString sampleName, TString MCDataPeriode, TString ExpUncertaintyName, TString WP, TString TOPCON) : fChain(0) {

// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
	std::cerr << "Error in EventLoop::EventLoop(): tree is nullptr" << std::endl;
	return;
   }

   if(WP == "85p"){
      m_btagCategoryBin        = 2;
   }
   if(WP == "77p"){
      m_btagCategoryBin        = 3;
   } 
   if(WP == "70p"){
      m_btagCategoryBin        = 4;
   }
   if(WP == "60p"){
      m_btagCategoryBin        = 5; 
   }
   //std::cout<<"Passed loop"<<std::endl;
   if(sampleName.Contains("tt_PP8.root") || sampleName.Contains("tt_PP8filtered.root") || sampleName.Contains("tt_4FS.root") || sampleName.Contains("tt_H7.root") || sampleName.Contains("tt_NLO.root"))
   {
     m_flag_tt = 1;
   }
   else
   {
    m_flag_tt = 0;
   }
   
   /*if(sampleName.Contains("tt_PP8.root") && MCDataPeriode == "MC16e")
   {
     m_scale = 0.5;
   }
   else
   {
    m_scale = 1;
   }*/


   if(TOPCON == "250"){
      m_topcon = 250;
   }
   if(TOPCON == "300"){
      m_topcon = 300;
   } 
   if(TOPCON == "350"){
      m_topcon = 350;
   }
   if(TOPCON == "200"){
      m_topcon = 200;
   }
   if(TOPCON == "225"){
      m_topcon = 225;
   } 
   if(TOPCON == "275"){
      m_topcon = 275;
   }
   if(TOPCON == "325"){
      m_topcon = 325;
   }

   if(TOPCON == "4"){
      m_topcon = 4;
   }
   if(TOPCON == "5"){
      m_topcon = 5;
   } 
   if(TOPCON == "6"){
      m_topcon = 6;
   }
   if(TOPCON == "7"){
      m_topcon = 7;
   }
   if(TOPCON == "8"){
      m_topcon = 8;
   } 
   if(TOPCON == "9"){
      m_topcon = 9;
   }
   if(TOPCON == "10"){
      m_topcon = 10;
   }
   if(TOPCON == "11"){
      m_topcon = 11;
   }
   if(TOPCON == "12"){
      m_topcon = 12;
   } 
   
   
   
   //std::cout<<"Passed flag set"<<std::endl;
   /*
   if(sampleName.Contains("HpWh_M250_L1.root")){
     m_weight_scale = 0.45835*(1/123037);
   }
   if(sampleName.Contains("HpWh_M300_L1.root")){
     m_weight_scale = 0.47379*(1/131096);
   }
   if(sampleName.Contains("HpWh_M350_L1.root")){
     m_weight_scale = 0.49374*(1/43684.1);
   }
   if(sampleName.Contains("HpWh_M400_L1.root")){
     m_weight_scale = 0.50747*(1/41528.7);
   }
   if(sampleName.Contains("HpWh_M500_L1.root")){
     m_weight_scale = 0.53407*(1/13160.5);
   }
   if(sampleName.Contains("HpWh_M600_L1.root")){
     m_weight_scale = 0.56197*(1/7112.87);
   }
   if(sampleName.Contains("HpWh_M700_L1.root")){
     m_weight_scale = 0.58076*(1/3706.68);
   }
   if(sampleName.Contains("HpWh_M800_L1.root")){
     m_weight_scale = 0.60086*(1/1841.95);
   }
   if(sampleName.Contains("HpWh_M900_L1.root")){
     m_weight_scale = 0.61551*(1/1269.09);
   }
   if(sampleName.Contains("HpWh_M1000_L1.root")){
     m_weight_scale = 0.63052*(1/822.079);
   }
   if(sampleName.Contains("HpWh_M1200_L1.root")){
     m_weight_scale = 0.65689*(1/304.93);
   } 
   if(sampleName.Contains("HpWh_M1400_L1.root")){
     m_weight_scale = 0.67148*(1/113.376);
   }
   if(sampleName.Contains("HpWh_M1600_L1.root")){
     m_weight_scale = 0.68918*(1/43.5164);
   }
   if(sampleName.Contains("HpWh_M1800_L1.root")){
     m_weight_scale = 0.70206*(1/33.2352);
   }
   if(sampleName.Contains("HpWh_M2000_L1.root")){
     m_weight_scale = 0.71196*(1/15.2037);
   }
   if(sampleName.Contains("HpWh_M2500_L1.root")){
     m_weight_scale = 0.73269*(1/3.2603);
   }
   */
   /*if(sampleName.Contains("HpWh_M3000_L1.root")){
     m_weight_scale = 0.74764;
   }*/

   Init(tree, sampleName, ExpUncertaintyName);
}

EventLoop::~EventLoop()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EventLoop::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EventLoop::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EventLoop::Init(TTree *tree, TString sampleName, TString ExpUncertaintyName){
   
   initializeMVA_qqbb();
   ///initializeMVA_lvbb();
  //std::cout<<"Passed Init 1"<<std::endl;
   m_NeutrinoBuilder = new NeutrinoBuilder("MeV");
   sampleName.Resize(sampleName.Index(".root"));

   m_UncNames   = {""};   
   //mySel        = {"Merged_LepP_SR","Merged_LepP_CR","Resolved_LepP_SR","Resolved_LepP_CR","Merged_LepN_SR","Merged_LepN_CR","Resolved_LepN_SR","Resolved_LepN_CR"};
   mySel        = {"Resolved_SR","Resolved_top"};
   double edges[40] = {0,50,100,150,200,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000};

   h_MET                    = new TH1Fs(sampleName+"_MET", "",        30, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Eta             = new TH1Fs(sampleName+"_Lepton_Eta", "", 25, -2.5, 2.5, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Pt              = new TH1Fs(sampleName+"_Lepton_Pt", "",  25, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags                 = new TH1Fs(sampleName+"_nBTags", "",     7, -0.5, 6.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_nJet                  = new TH1Fs(sampleName+"_nJet", "",      13, -0.5, 12.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt                    = new TH1Fs(sampleName+"_Mwt", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_MinDeltaPhiJETMET      = new TH1Fs(sampleName+"_MinDeltaPhiJETMET", "", 32,  0, 3.2,  mySel, m_UncNames, ExpUncertaintyName);
   h_lepch                  = new TH1Fs(sampleName+"_LepCharge", "", 20,  -2, 2,  mySel, m_UncNames, ExpUncertaintyName);
   h_dRq1j1                 = new TH1Fs(sampleName+"_dRq1j1", "", 100,  0, 5,  mySel, m_UncNames, ExpUncertaintyName);
   h_dRq2j2                 = new TH1Fs(sampleName+"_dRq2j2", "", 100,  0, 5,  mySel, m_UncNames, ExpUncertaintyName);
   h_dRb1j1                 = new TH1Fs(sampleName+"_dRb1j1", "", 100,  0, 5,  mySel, m_UncNames, ExpUncertaintyName);
   h_dRb2j2                = new TH1Fs(sampleName+"_dRb2j2", "", 100,  0, 5,  mySel, m_UncNames, ExpUncertaintyName);
   
   h_dRHjj                 = new TH1Fs(sampleName+"_dRHjj", "", 100,  0, 5,  mySel, m_UncNames, ExpUncertaintyName);
   h_dRWjj                 = new TH1Fs(sampleName+"_dRWjj", "", 100,  0, 5,  mySel, m_UncNames, ExpUncertaintyName);

   h_Wlep_pT                = new TH1Fs(sampleName+"_WleppT", "", 40,  0, 2500,  mySel, m_UncNames, ExpUncertaintyName);
   h_Wlep_eta               = new TH1Fs(sampleName+"_Wlepeta", "", 55,  -5.5, 5.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Wlep_phi               = new TH1Fs(sampleName+"_Wlepphi", "", 25,  -3.2, 3.2,  mySel, m_UncNames, ExpUncertaintyName);
 
   h_Higgs_pT                = new TH1Fs(sampleName+"_HiggspT", "", 40,  0, 2500,  mySel, m_UncNames, ExpUncertaintyName);
   h_Higgs_eta               = new TH1Fs(sampleName+"_Higgseta", "", 55,  -5.5, 5.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Higgs_phi               = new TH1Fs(sampleName+"_Higgsphi", "", 25,  -3.2, 3.2,  mySel, m_UncNames, ExpUncertaintyName);

   h_Whad_pT                = new TH1Fs(sampleName+"_WhadpT", "", 40,  0, 2500,  mySel, m_UncNames, ExpUncertaintyName);
   h_Whad_eta               = new TH1Fs(sampleName+"_Whadeta", "", 55,  -5.5, 5.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Whad_phi               = new TH1Fs(sampleName+"_Whadphi", "", 25,  -3.2, 3.2,  mySel, m_UncNames, ExpUncertaintyName);

   h_HT                     = new TH1Fs(sampleName+"_HT", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all                 = new TH1Fs(sampleName+"_HT_all", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_ntup            = new TH1Fs(sampleName+"_HT_all_ntup",39, edges,    mySel, m_UncNames, ExpUncertaintyName);

   h_HT_5j                  = new TH1Fs(sampleName+"_HT_5j",39, edges,     mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j                = new TH1Fs(sampleName+"_HT_all_5j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j                  = new TH1Fs(sampleName+"_HT_6j",39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j                = new TH1Fs(sampleName+"_HT_all_6j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j                  = new TH1Fs(sampleName+"_HT_7j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j                = new TH1Fs(sampleName+"_HT_all_7j", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j                  = new TH1Fs(sampleName+"_HT_8j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j                = new TH1Fs(sampleName+"_HT_all_8j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j                  = new TH1Fs(sampleName+"_HT_9j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j                = new TH1Fs(sampleName+"_HT_all_9j",39, edges,   mySel, m_UncNames, ExpUncertaintyName);

   h_HT_bjets               = new TH1Fs(sampleName+"_HT_bjets", "",  30, 0, 1200,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH                    = new TH1Fs(sampleName+"_mVH", "",       16, 0, 2.4,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_true               = new TH1Fs(sampleName+"_mVH_true", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW            = new TH1Fs(sampleName+"_DeltaPhi_HW", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW            = new TH1Fs(sampleName+"_DeltaEta_HW", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse         = new TH1Fs(sampleName+"_maxMVAResponse", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH                    = new TH1Fs(sampleName+"_pTH", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_over_mVH           = new TH1Fs(sampleName+"_pTH_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus                = new TH1Fs(sampleName+"_pTWplus", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTW_over_mVH      	    = new TH1Fs(sampleName+"_pTW_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWminus               = new TH1Fs(sampleName+"_pTWminus", "",25, 0, 800,    mySel, m_UncNames, ExpUncertaintyName);
   h_massWminus               = new TH1Fs(sampleName+"_massWminus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_etaWminus               = new TH1Fs(sampleName+"_etaWminus", "",55, -5.5, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_phiWminus               = new TH1Fs(sampleName+"_phiWminus", "",25, -3.2, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_mH                     = new TH1Fs(sampleName+"_mH", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName); 
   h_mWplus                 = new TH1Fs(sampleName+"_mWplus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_mWinus                 = new TH1Fs(sampleName+"_mWinus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_tagCategory            = new TH1Fs(sampleName+"_BtagCategory", "",11, -0.5, 10.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution        = new TH1Fs(sampleName+"_mass_resolution", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);

   h_mass_resolution_MVA07  = new TH1Fs(sampleName+"_mass_resolution_MVA07", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_MVA08  = new TH1Fs(sampleName+"_mass_resolution_MVA08", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_MVA09  = new TH1Fs(sampleName+"_mass_resolution_MVA09", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_MVA095 = new TH1Fs(sampleName+"_mass_resolution_MVA095", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);

   h_mass_resolution_200 = new TH1Fs(sampleName+"_mass_resolution_200", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_225 = new TH1Fs(sampleName+"_mass_resolution_225", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_250 = new TH1Fs(sampleName+"_mass_resolution_250", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_275 = new TH1Fs(sampleName+"_mass_resolution_275", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);

   
   h_pTH_250Res                    = new TH1Fs(sampleName+"_pTH_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_250Res               = new TH1Fs(sampleName+"_pTWplus_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_250Res         = new TH1Fs(sampleName+"_maxMVAResponse_250Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_400Res                    = new TH1Fs(sampleName+"_pTH_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_400Res               = new TH1Fs(sampleName+"_pTWplus_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_400Res         = new TH1Fs(sampleName+"_maxMVAResponse_400Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_800Res                    = new TH1Fs(sampleName+"_pTH_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_800Res               = new TH1Fs(sampleName+"_pTWplus_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_800Res         = new TH1Fs(sampleName+"_maxMVAResponse_800Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1200Res                    = new TH1Fs(sampleName+"_pTH_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1200Res               = new TH1Fs(sampleName+"_pTWplus_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1200Res         = new TH1Fs(sampleName+"_maxMVAResponse_1200Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1600Res                    = new TH1Fs(sampleName+"_pTH_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1600Res               = new TH1Fs(sampleName+"_pTWplus_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1600Res         = new TH1Fs(sampleName+"_maxMVAResponse_1600Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_pTH_2000Res                    = new TH1Fs(sampleName+"_pTH_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_2000Res               = new TH1Fs(sampleName+"_pTWplus_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_2000Res         = new TH1Fs(sampleName+"_maxMVAResponse_2000Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_pTH_250Res_1b                    = new TH1Fs(sampleName+"_1B_pTH_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_250Res_1b               = new TH1Fs(sampleName+"_1B_pTWplus_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_250Res_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_250Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1200Res_1b                    = new TH1Fs(sampleName+"_1B_pTH_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1200Res_1b               = new TH1Fs(sampleName+"_1B_pTWplus_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1200Res_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_1200Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_2000Res_1b                    = new TH1Fs(sampleName+"_1B_pTH_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_2000Res_1b               = new TH1Fs(sampleName+"_1B_pTWplus_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_2000Res_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_2000Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_400Res_1b                    = new TH1Fs(sampleName+"_1B_pTH_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_400Res_1b               = new TH1Fs(sampleName+"_1B_pTWplus_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_400Res_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_400Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_800Res_1b                    = new TH1Fs(sampleName+"_1B_pTH_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_800Res_1b               = new TH1Fs(sampleName+"_1B_pTWplus_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_800Res_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_800Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1600Res_1b                    = new TH1Fs(sampleName+"_1B_pTH_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1600Res_1b              = new TH1Fs(sampleName+"_1B_pTWplus_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1600Res_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_1600Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);


   h_pTH_250Res_1c                    = new TH1Fs(sampleName+"_1C_pTH_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_250Res_1c               = new TH1Fs(sampleName+"_1C_pTWplus_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_250Res_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_250Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1200Res_1c                    = new TH1Fs(sampleName+"_1C_pTH_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1200Res_1c               = new TH1Fs(sampleName+"_1C_pTWplus_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1200Res_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_1200Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_2000Res_1c                    = new TH1Fs(sampleName+"_1C_pTH_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_2000Res_1c               = new TH1Fs(sampleName+"_1C_pTWplus_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_2000Res_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_2000Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);


   h_pTH_400Res_1c                    = new TH1Fs(sampleName+"_1C_pTH_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_400Res_1c               = new TH1Fs(sampleName+"_1C_pTWplus_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_400Res_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_400Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_800Res_1c                    = new TH1Fs(sampleName+"_1C_pTH_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_800Res_1c               = new TH1Fs(sampleName+"_1C_pTWplus_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_800Res_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_800Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1600Res_1c                    = new TH1Fs(sampleName+"_1C_pTH_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1600Res_1c              = new TH1Fs(sampleName+"_1C_pTWplus_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1600Res_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_1600Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);


   h_pTH_250Res_1l                    = new TH1Fs(sampleName+"_1L_pTH_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_250Res_1l               = new TH1Fs(sampleName+"_1L_pTWplus_250Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_250Res_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_250Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1200Res_1l                    = new TH1Fs(sampleName+"_1L_pTH_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1200Res_1l               = new TH1Fs(sampleName+"_1L_pTWplus_1200Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1200Res_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_1200Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_2000Res_1l                    = new TH1Fs(sampleName+"_1L_pTH_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_2000Res_1l               = new TH1Fs(sampleName+"_1L_pTWplus_2000Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_2000Res_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_2000Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);


   h_pTH_400Res_1l                    = new TH1Fs(sampleName+"_1L_pTH_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_400Res_1l               = new TH1Fs(sampleName+"_1L_pTWplus_400Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_400Res_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_400Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_800Res_1l                    = new TH1Fs(sampleName+"_1L_pTH_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_800Res_1l               = new TH1Fs(sampleName+"_1L_pTWplus_800Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_800Res_1l        = new TH1Fs(sampleName+"_1L_maxMVAResponse_800Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_1600Res_1l                   = new TH1Fs(sampleName+"_1L_pTH_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1600Res_1l              = new TH1Fs(sampleName+"_1L_pTWplus_1600Res", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1600Res_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_1600Res", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_DeltaPhi_HW_5j           = new TH1Fs(sampleName+"_DeltaPhi_HW_5j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_5j            = new TH1Fs(sampleName+"_DeltaEta_HW_5j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_5j_1b           = new TH1Fs(sampleName+"_1B_DeltaPhi_HW_5j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_5j_1b           = new TH1Fs(sampleName+"_1B_DeltaEta_HW_5j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_5j_1c           = new TH1Fs(sampleName+"_1C_DeltaPhi_HW_5j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_5j_1c           = new TH1Fs(sampleName+"_1C_DeltaEta_HW_5j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_5j_1l          = new TH1Fs(sampleName+"_1L_DeltaPhi_HW_5j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_5j_1l           = new TH1Fs(sampleName+"_1L_DeltaEta_HW_5j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_Mwt_5j                    = new TH1Fs(sampleName+"_Mwt_5j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_6j                    = new TH1Fs(sampleName+"_Mwt_6j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_7j                    = new TH1Fs(sampleName+"_Mwt_7j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_8j                    = new TH1Fs(sampleName+"_Mwt_8j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_9j                    = new TH1Fs(sampleName+"_Mwt_9j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);

   h_Mwt_5j_1b                   = new TH1Fs(sampleName+"_1B_Mwt_5j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_6j_1b                    = new TH1Fs(sampleName+"_1B_Mwt_6j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_7j_1b                    = new TH1Fs(sampleName+"_1B_Mwt_7j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_8j_1b                   = new TH1Fs(sampleName+"_1B_Mwt_8j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_9j_1b                    = new TH1Fs(sampleName+"_1B_Mwt_9j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);

   h_Mwt_5j_1c                  = new TH1Fs(sampleName+"_1C_Mwt_5j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_6j_1c                   = new TH1Fs(sampleName+"_1C_Mwt_6j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_7j_1c                   = new TH1Fs(sampleName+"_1C_Mwt_7j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_8j_1c                  = new TH1Fs(sampleName+"_1C_Mwt_8j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_9j_1c                    = new TH1Fs(sampleName+"_1C_Mwt_9j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);

   h_Mwt_5j_1l                   = new TH1Fs(sampleName+"_1L_Mwt_5j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_6j_1l                    = new TH1Fs(sampleName+"_1L_Mwt_6j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_7j_1l                    = new TH1Fs(sampleName+"_1L_Mwt_7j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_8j_1l                   = new TH1Fs(sampleName+"_1L_Mwt_8j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_9j_1l                    = new TH1Fs(sampleName+"_1L_Mwt_9j", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_5j_1b                   = new TH1Fs(sampleName+"_1B_mVH_5j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_5j_1c                   = new TH1Fs(sampleName+"_1C_mVH_5j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_5j_1l                   = new TH1Fs(sampleName+"_1L_mVH_5j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_6j_1b                   = new TH1Fs(sampleName+"_1B_mVH_6j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_6j_1c                   = new TH1Fs(sampleName+"_1C_mVH_6j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_6j_1l                   = new TH1Fs(sampleName+"_1L_mVH_6j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_7j_1b                   = new TH1Fs(sampleName+"_1B_mVH_7j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_7j_1c                   = new TH1Fs(sampleName+"_1C_mVH_7j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_7j_1l                   = new TH1Fs(sampleName+"_1L_mVH_7j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_mVH_8j_1b                   = new TH1Fs(sampleName+"_1B_mVH_8j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8j_1c                   = new TH1Fs(sampleName+"_1C_mVH_8j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8j_1l                   = new TH1Fs(sampleName+"_1L_mVH_8j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_9j_1b                   = new TH1Fs(sampleName+"_1B_mVH_9j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9j_1c                   = new TH1Fs(sampleName+"_1C_mVH_9j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9j_1l                   = new TH1Fs(sampleName+"_1L_mVH_9j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_mVH_5j                   = new TH1Fs(sampleName+"_mVH_5j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_6j                   = new TH1Fs(sampleName+"_mVH_6j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_7j                   = new TH1Fs(sampleName+"_mVH_7j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8j                   = new TH1Fs(sampleName+"_mVH_8j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9j                   = new TH1Fs(sampleName+"_mVH_9j", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);

   h_DeltaPhi_HW_6j           = new TH1Fs(sampleName+"_DeltaPhi_HW_6j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_6j            = new TH1Fs(sampleName+"_DeltaEta_HW_6j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_6j_1b           = new TH1Fs(sampleName+"_1B_DeltaPhi_HW_6j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_6j_1b           = new TH1Fs(sampleName+"_1B_DeltaEta_HW_6j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_6j_1c           = new TH1Fs(sampleName+"_1C_DeltaPhi_HW_6j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_6j_1c           = new TH1Fs(sampleName+"_1C_DeltaEta_HW_6j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_6j_1l          = new TH1Fs(sampleName+"_1L_DeltaPhi_HW_6j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_6j_1l           = new TH1Fs(sampleName+"_1L_DeltaEta_HW_6j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_DeltaPhi_HW_7j           = new TH1Fs(sampleName+"_DeltaPhi_HW_7j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_7j            = new TH1Fs(sampleName+"_DeltaEta_HW_7j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_7j_1b           = new TH1Fs(sampleName+"_1B_DeltaPhi_HW_7j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_7j_1b           = new TH1Fs(sampleName+"_1B_DeltaEta_HW_7j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_7j_1c           = new TH1Fs(sampleName+"_1C_DeltaPhi_HW_7j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_7j_1c           = new TH1Fs(sampleName+"_1C_DeltaEta_HW_7j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_7j_1l          = new TH1Fs(sampleName+"_1L_DeltaPhi_HW_7j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_7j_1l           = new TH1Fs(sampleName+"_1L_DeltaEta_HW_7j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_DeltaPhi_HW_8j           = new TH1Fs(sampleName+"_DeltaPhi_HW_8j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_8j            = new TH1Fs(sampleName+"_DeltaEta_HW_8j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_8j_1b           = new TH1Fs(sampleName+"_1B_DeltaPhi_HW_8j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_8j_1b           = new TH1Fs(sampleName+"_1B_DeltaEta_HW_8j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_8j_1c           = new TH1Fs(sampleName+"_1C_DeltaPhi_HW_8j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_8j_1c           = new TH1Fs(sampleName+"_1C_DeltaEta_HW_8j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_8j_1l          = new TH1Fs(sampleName+"_1L_DeltaPhi_HW_8j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_8j_1l           = new TH1Fs(sampleName+"_1L_DeltaEta_HW_8j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_DeltaPhi_HW_9j           = new TH1Fs(sampleName+"_DeltaPhi_HW_9j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_9j            = new TH1Fs(sampleName+"_DeltaEta_HW_9j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_9j_1b           = new TH1Fs(sampleName+"_1B_DeltaPhi_HW_9j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_9j_1b           = new TH1Fs(sampleName+"_1B_DeltaEta_HW_9j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_9j_1c           = new TH1Fs(sampleName+"_1C_DeltaPhi_HW_9j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_9j_1c           = new TH1Fs(sampleName+"_1C_DeltaEta_HW_9j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_9j_1l          = new TH1Fs(sampleName+"_1L_DeltaPhi_HW_9j", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_9j_1l           = new TH1Fs(sampleName+"_1L_DeltaEta_HW_9j", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_5j                   = new TH1Fs(sampleName+"_pTH_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_5j                = new TH1Fs(sampleName+"_pTWplus_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_5j_1b                   = new TH1Fs(sampleName+"_1B_pTH_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_5j_1b               = new TH1Fs(sampleName+"_1B_pTWplus_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_5j_1c                   = new TH1Fs(sampleName+"_1C_pTH_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_5j_1c               = new TH1Fs(sampleName+"_1C_pTWplus_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_5j_1l                   = new TH1Fs(sampleName+"_1L_pTH_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_5j_1l              = new TH1Fs(sampleName+"_1L_pTWplus_5j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_6j                   = new TH1Fs(sampleName+"_pTH_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_6j                = new TH1Fs(sampleName+"_pTWplus_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_6j_1b                   = new TH1Fs(sampleName+"_1B_pTH_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_6j_1b               = new TH1Fs(sampleName+"_1B_pTWplus_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_6j_1c                   = new TH1Fs(sampleName+"_1C_pTH_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_6j_1c               = new TH1Fs(sampleName+"_1C_pTWplus_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_6j_1l                   = new TH1Fs(sampleName+"_1L_pTH_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_6j_1l              = new TH1Fs(sampleName+"_1L_pTWplus_6j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_7j                   = new TH1Fs(sampleName+"_pTH_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_7j                = new TH1Fs(sampleName+"_pTWplus_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_7j_1b                   = new TH1Fs(sampleName+"_1B_pTH_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_7j_1b               = new TH1Fs(sampleName+"_1B_pTWplus_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_7j_1c                   = new TH1Fs(sampleName+"_1C_pTH_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_7j_1c               = new TH1Fs(sampleName+"_1C_pTWplus_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_7j_1l                   = new TH1Fs(sampleName+"_1L_pTH_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_7j_1l              = new TH1Fs(sampleName+"_1L_pTWplus_7j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_8j                   = new TH1Fs(sampleName+"_pTH_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_8j                = new TH1Fs(sampleName+"_pTWplus_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_8j_1b                   = new TH1Fs(sampleName+"_1B_pTH_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_8j_1b               = new TH1Fs(sampleName+"_1B_pTWplus_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_8j_1c                   = new TH1Fs(sampleName+"_1C_pTH_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_8j_1c               = new TH1Fs(sampleName+"_1C_pTWplus_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_8j_1l                   = new TH1Fs(sampleName+"_1L_pTH_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_8j_1l              = new TH1Fs(sampleName+"_1L_pTWplus_8j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);

   h_pTH_9j                   = new TH1Fs(sampleName+"_pTH_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_9j                = new TH1Fs(sampleName+"_pTWplus_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_9j_1b                   = new TH1Fs(sampleName+"_1B_pTH_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_9j_1b               = new TH1Fs(sampleName+"_1B_pTWplus_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_9j_1c                   = new TH1Fs(sampleName+"_1C_pTH_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_9j_1c               = new TH1Fs(sampleName+"_1C_pTWplus_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_9j_1l                   = new TH1Fs(sampleName+"_1L_pTH_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_9j_1l              = new TH1Fs(sampleName+"_1L_pTWplus_9j", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);

   h_maxMVAResponse_5j         = new TH1Fs(sampleName+"_maxMVAResponse_5j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_6j         = new TH1Fs(sampleName+"_maxMVAResponse_6j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_7j         = new TH1Fs(sampleName+"_maxMVAResponse_7j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_8j         = new TH1Fs(sampleName+"_maxMVAResponse_8j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_9j         = new TH1Fs(sampleName+"_maxMVAResponse_9j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_maxMVAResponse_5j_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_5j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_6j_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_6j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_7j_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_7j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_8j_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_8j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_9j_1b         = new TH1Fs(sampleName+"_1B_maxMVAResponse_9j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_maxMVAResponse_5j_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_5j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_6j_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_6j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_7j_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_7j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_8j_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_8j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_9j_1c         = new TH1Fs(sampleName+"_1C_maxMVAResponse_9j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_maxMVAResponse_5j_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_5j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_6j_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_6j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_7j_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_7j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_8j_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_8j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_9j_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse_9j", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   

   //h_top_mass_resolution    = new TH1Fs(sampleName+"_top_mass_resolution", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   //h_had_t                  = new TH1Fs(sampleName+"_mass_resolution", "",20, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass           = new TH1Fs(sampleName+"_lep_top_mass", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT             = new TH1Fs(sampleName+"_lep_top_pT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minpT           = new TH1Fs(sampleName+"_lep_top_mass_minpT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minpT             = new TH1Fs(sampleName+"_lep_top_pT_minpT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minXwT           = new TH1Fs(sampleName+"_lep_top_mass_minXwT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minXwT            = new TH1Fs(sampleName+"_lep_top_pT_minXwT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);

   h_evw                    = new TH1Fs(sampleName+"_evw", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_lvbb               = new TH1Fs(sampleName+"_evw_lvbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_qqbb               = new TH1Fs(sampleName+"_evw_qqbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_leclvbb            = new TH1Fs(sampleName+"_evw_leclvbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_lecqqbb            = new TH1Fs(sampleName+"_evw_lecqqbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Genlvbb            = new TH1Fs(sampleName+"_evw_Genlvbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Genqqbb            = new TH1Fs(sampleName+"_evw_Genqqbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_neglep             = new TH1Fs(sampleName+"_evw_neglep", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_poslep             = new TH1Fs(sampleName+"_evw_poslep", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_evw_MVA                = new TH1Fs(sampleName+"_evw_MVA", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_bdt          = new TH1Fs(sampleName+"_evw_trueM_bdt", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu            = new TH1Fs(sampleName+"_evw_MVAaccu", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu_MVA07            = new TH1Fs(sampleName+"_evw_MVAaccu_MVA07", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu_MVA08          = new TH1Fs(sampleName+"_evw_MVAaccu_MVA08", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu_MVA09          = new TH1Fs(sampleName+"_evw_MVAaccu_MVA09", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu_MVA095            = new TH1Fs(sampleName+"_evw_MVAaccu_MVA095", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_evw_MVAaccu2            = new TH1Fs(sampleName+"_evw_MVAaccu2", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu4            = new TH1Fs(sampleName+"_evw_MVAaccu4", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu5            = new TH1Fs(sampleName+"_evw_MVAaccu5", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_MVAaccu6            = new TH1Fs(sampleName+"_evw_MVAaccu6", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_eone        = new TH1Fs(sampleName+"_evw_trueM_eone", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_eone_MVA07        = new TH1Fs(sampleName+"_evw_trueM_eone_MVA07", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_eone_MVA08        = new TH1Fs(sampleName+"_evw_trueM_eone_MVA08", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_eone_MVA09        = new TH1Fs(sampleName+"_evw_trueM_eone_MVA09", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_eone_MVA095        = new TH1Fs(sampleName+"_evw_trueM_eone_MVA095", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_gefour       = new TH1Fs(sampleName+"_evw_trueM_gefour", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_etwo         = new TH1Fs(sampleName+"_evw_trueM_etwo", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_ethree       = new TH1Fs(sampleName+"_evw_trueM_ethree", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_trueM_eone_lvbb    = new TH1Fs(sampleName+"_evw_trueM_eone_lvbb", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_j1j2               = new TH1Fs(sampleName+"_evw_j1j2", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_j3j4               = new TH1Fs(sampleName+"_evw_j3j4", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName); 
   h_evw_Whad               = new TH1Fs(sampleName+"_evw_Whad", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Whad_MVA07              = new TH1Fs(sampleName+"_evw_Whad_MVA07", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Whad_MVA08              = new TH1Fs(sampleName+"_evw_Whad_MVA08", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Whad_MVA09              = new TH1Fs(sampleName+"_evw_Whad_MVA09", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Whad_MVA095              = new TH1Fs(sampleName+"_evw_Whad_MVA095", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_Wlep              = new TH1Fs(sampleName+"_evw_Wlep", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_evw_qqbbrec           = new TH1Fs(sampleName+"_evw_qqbbrec", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_abstop_res             = new TH1Fs(sampleName+"_abstop_resolution", "",500, 0, 5000,    mySel, m_UncNames, ExpUncertaintyName);
    //std::cout<<"Passed Init 2"<<std::endl;
   h_MET_1b                   = new TH1Fs(sampleName+"_1B_MET", "",        30, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Eta_1b             = new TH1Fs(sampleName+"_1B_Lepton_Eta", "", 25, -2.5, 2.5, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Pt_1b             = new TH1Fs(sampleName+"_1B_Lepton_Pt", "",  25, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags_1b                = new TH1Fs(sampleName+"_1B_nBTags", "",     7, -0.5, 6.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_nJet_1b                 = new TH1Fs(sampleName+"_1B_nJet", "",      13, -0.5, 12.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_1b                    = new TH1Fs(sampleName+"_1B_Mwt", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_MinDeltaPhiJETMET_1b      = new TH1Fs(sampleName+"_1B_MinDeltaPhiJETMET", "", 32,  0, 3.2,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_1b                     = new TH1Fs(sampleName+"_1B_HT", "",        80, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_1b                 = new TH1Fs(sampleName+"_1B_HT_all", "",        80, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_HT_all_ntup_1b           = new TH1Fs(sampleName+"_1B_HT_all_ntup", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_ntup_1c           = new TH1Fs(sampleName+"_1C_HT_all_ntup",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_ntup_1l           = new TH1Fs(sampleName+"_1L_HT_all_ntup",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);

   h_HT_5j_1b                  = new TH1Fs(sampleName+"_HT_5j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j_1b              = new TH1Fs(sampleName+"_HT_all_5j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j_1b                  = new TH1Fs(sampleName+"_HT_6j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j_1b               = new TH1Fs(sampleName+"_HT_all_6j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j_1b                  = new TH1Fs(sampleName+"_HT_7j_1b",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j_1b               = new TH1Fs(sampleName+"_HT_all_7j_1b", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j_1b                  = new TH1Fs(sampleName+"_HT_8j_1b",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j_1b                = new TH1Fs(sampleName+"_HT_all_8j_1b", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j_1b                  = new TH1Fs(sampleName+"_HT_9j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j_1b               = new TH1Fs(sampleName+"_HT_all_9j_1b", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);

   h_HT_bjets_1b              = new TH1Fs(sampleName+"_1B_HT_bjets", "",  30, 0, 1200,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_1b                    = new TH1Fs(sampleName+"_1B_mVH", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_1b            = new TH1Fs(sampleName+"_1B_DeltaPhi_HW", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_1b           = new TH1Fs(sampleName+"_1B_DeltaEta_HW", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1b        = new TH1Fs(sampleName+"_1B_maxMVAResponse", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_1b                    = new TH1Fs(sampleName+"_1B_pTH", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_over_mVH_1b          = new TH1Fs(sampleName+"_1B_pTH_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1b               = new TH1Fs(sampleName+"_1B_pTWplus", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTW_over_mVH_1b      	    = new TH1Fs(sampleName+"_1B_pTW_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWminus_1b               = new TH1Fs(sampleName+"_1B_pTWminus", "",25, 0, 800,    mySel, m_UncNames, ExpUncertaintyName);
   h_massWminus_1b              = new TH1Fs(sampleName+"_1B_massWminus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_etaWminus_1b               = new TH1Fs(sampleName+"_1B_etaWminus", "",55, -5.5, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_phiWminus_1b               = new TH1Fs(sampleName+"_1B_phiWminus", "",25, -3.2, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_mH_1b                     = new TH1Fs(sampleName+"_1B_mH", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName); 
   h_mWplus_1b                 = new TH1Fs(sampleName+"_1B_mWplus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_tagCategory_1b            = new TH1Fs(sampleName+"_1B_BtagCategory", "",11, -0.5, 10.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_1b        = new TH1Fs(sampleName+"_1B_mass_resolution", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_1b          = new TH1Fs(sampleName+"_1B_lep_top_mass", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_1b             = new TH1Fs(sampleName+"_1B_lep_top_pT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minpT_1b           = new TH1Fs(sampleName+"_1B_lep_top_mass_minpT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minpT_1b             = new TH1Fs(sampleName+"_1B_lep_top_pT_minpT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minXwT_1b          = new TH1Fs(sampleName+"_1B_lep_top_mass_minXwT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minXwT_1b            = new TH1Fs(sampleName+"_1B_lep_top_pT_minXwT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   //std::cout<<"Passed Init 3"<<std::endl;
   h_MET_1c                   = new TH1Fs(sampleName+"_1C_MET", "",        30, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Eta_1c             = new TH1Fs(sampleName+"_1C_Lepton_Eta", "", 25, -2.5, 2.5, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Pt_1c             = new TH1Fs(sampleName+"_1C_Lepton_Pt", "",  25, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags_1c                 = new TH1Fs(sampleName+"_1C_nBTags", "",     7, -0.5, 6.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_nJet_1c                  = new TH1Fs(sampleName+"_1C_nJet", "",      13, -0.5, 12.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_1c                    = new TH1Fs(sampleName+"_1C_Mwt", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_MinDeltaPhiJETMET_1c      = new TH1Fs(sampleName+"_1C_MinDeltaPhiJETMET", "", 32,  0, 3.2,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_1c                     = new TH1Fs(sampleName+"_1C_HT", "",        80, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_1c                 = new TH1Fs(sampleName+"_1C_HT_all", "",        80, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);

   h_HT_5j_1c                 = new TH1Fs(sampleName+"_HT_5j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j_1c              = new TH1Fs(sampleName+"_HT_all_5j_1c",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j_1c                  = new TH1Fs(sampleName+"_HT_6j_1c", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j_1c               = new TH1Fs(sampleName+"_HT_all_6j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j_1c                 = new TH1Fs(sampleName+"_HT_7j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j_1c               = new TH1Fs(sampleName+"_HT_all_7j_1c", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j_1c                  = new TH1Fs(sampleName+"_HT_8j_1c", 39, edges, mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j_1c               = new TH1Fs(sampleName+"_HT_all_8j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j_1c                 = new TH1Fs(sampleName+"_HT_9j_1c", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j_1c               = new TH1Fs(sampleName+"_HT_all_9j_1c",39, edges,  mySel, m_UncNames, ExpUncertaintyName);

   h_HT_bjets_1c               = new TH1Fs(sampleName+"_1C_HT_bjets", "",  30, 0, 1200,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_1c                    = new TH1Fs(sampleName+"_1C_mVH", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_1c            = new TH1Fs(sampleName+"_1C_DeltaPhi_HW", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_1c            = new TH1Fs(sampleName+"_1C_DeltaEta_HW", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1c        = new TH1Fs(sampleName+"_1C_maxMVAResponse", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_1c                    = new TH1Fs(sampleName+"_1C_pTH", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_over_mVH_1c           = new TH1Fs(sampleName+"_1C_pTH_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1c                = new TH1Fs(sampleName+"_1C_pTWplus", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTW_over_mVH_1c     	    = new TH1Fs(sampleName+"_1C_pTW_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWminus_1c              = new TH1Fs(sampleName+"_1C_pTWminus", "",25, 0, 800,    mySel, m_UncNames, ExpUncertaintyName);
   h_massWminus_1c              = new TH1Fs(sampleName+"_1C_massWminus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_etaWminus_1c               = new TH1Fs(sampleName+"_1C_etaWminus", "",55, -5.5, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_phiWminus_1c               = new TH1Fs(sampleName+"_1C_phiWminus", "",25, -3.2, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_mH_1c                     = new TH1Fs(sampleName+"_1C_mH", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName); 
   h_mWplus_1c                 = new TH1Fs(sampleName+"_1C_mWplus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_tagCategory_1c           = new TH1Fs(sampleName+"_1C_BtagCategory", "",11, -0.5, 10.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_1c        = new TH1Fs(sampleName+"_1C_mass_resolution", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_1c          = new TH1Fs(sampleName+"_1C_lep_top_mass", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_1c             = new TH1Fs(sampleName+"_1C_lep_top_pT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minpT_1c           = new TH1Fs(sampleName+"_1C_lep_top_mass_minpT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minpT_1c            = new TH1Fs(sampleName+"_1C_lep_top_pT_minpT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minXwT_1c          = new TH1Fs(sampleName+"_1C_lep_top_mass_minXwT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minXwT_1c            = new TH1Fs(sampleName+"_1C_lep_top_pT_minXwT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);

   h_MET_1l                    = new TH1Fs(sampleName+"_1L_MET", "",        30, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Eta_1l             = new TH1Fs(sampleName+"_1L_Lepton_Eta", "", 25, -2.5, 2.5, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Pt_1l             = new TH1Fs(sampleName+"_1L_Lepton_Pt", "",  25, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags_1l                 = new TH1Fs(sampleName+"_1L_nBTags", "",     7, -0.5, 6.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_nJet_1l                  = new TH1Fs(sampleName+"_1L_nJet", "",      13, -0.5, 12.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_1l                    = new TH1Fs(sampleName+"_1L_Mwt", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_MinDeltaPhiJETMET_1l      = new TH1Fs(sampleName+"_1L_MinDeltaPhiJETMET", "", 32,  0, 3.2,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_1l                 = new TH1Fs(sampleName+"_1L_HT_all", "",        80, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_1l                     = new TH1Fs(sampleName+"_1L_HT", "",        80, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);

   h_HT_5j_1l                 = new TH1Fs(sampleName+"_HT_5j_1l", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j_1l             = new TH1Fs(sampleName+"_HT_all_5j_1l",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j_1l                 = new TH1Fs(sampleName+"_HT_6j_1l", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j_1l               = new TH1Fs(sampleName+"_HT_all_6j_1l", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j_1l                 = new TH1Fs(sampleName+"_HT_7j_1l", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j_1l              = new TH1Fs(sampleName+"_HT_all_7j_1l", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j_1l                  = new TH1Fs(sampleName+"_HT_8j_1l", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j_1l               = new TH1Fs(sampleName+"_HT_all_8j_1l",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j_1l                 = new TH1Fs(sampleName+"_HT_9j_1l",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j_1l               = new TH1Fs(sampleName+"_HT_all_9j_1l",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);

   h_HT_bjets_1l               = new TH1Fs(sampleName+"_1L_HT_bjets", "",  30, 0, 1200,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_1l                   = new TH1Fs(sampleName+"_1L_mVH", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_1l            = new TH1Fs(sampleName+"_1L_DeltaPhi_HW", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_1l            = new TH1Fs(sampleName+"_1L_DeltaEta_HW", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_1l         = new TH1Fs(sampleName+"_1L_maxMVAResponse", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_1l                   = new TH1Fs(sampleName+"_1L_pTH", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_over_mVH_1l          = new TH1Fs(sampleName+"_1L_pTH_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_1l                = new TH1Fs(sampleName+"_1L_pTWplus", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTW_over_mVH_1l     	    = new TH1Fs(sampleName+"_1L_pTW_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWminus_1l               = new TH1Fs(sampleName+"_1L_pTWminus", "",25, 0, 800,    mySel, m_UncNames, ExpUncertaintyName);
   h_massWminus_1l             = new TH1Fs(sampleName+"_1L_massWminus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_etaWminus_1l              = new TH1Fs(sampleName+"_1L_etaWminus", "",55, -5.5, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_phiWminus_1l               = new TH1Fs(sampleName+"_1L_phiWminus", "",25, -3.2, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_mH_1l                     = new TH1Fs(sampleName+"_1L_mH", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName); 
   h_mWplus_1l                 = new TH1Fs(sampleName+"_1L_mWplus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_tagCategory_1l           = new TH1Fs(sampleName+"_1L_BtagCategory", "",11, -0.5, 10.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_1l        = new TH1Fs(sampleName+"_1L_mass_resolution", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_1l          = new TH1Fs(sampleName+"_1L_lep_top_mass", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_1l             = new TH1Fs(sampleName+"_1L_lep_top_pT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minpT_1l           = new TH1Fs(sampleName+"_1L_lep_top_mass_minpT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minpT_1l            = new TH1Fs(sampleName+"_1L_lep_top_pT_minpT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_mass_minXwT_1l         = new TH1Fs(sampleName+"_1L_lep_top_mass_minXwT", "",20, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_lep_top_pT_minXwT_1l           = new TH1Fs(sampleName+"_1L_lep_top_pT_minXwT", "",30, 0, 1500,    mySel, m_UncNames, ExpUncertaintyName);

   h_MET_2sig                    = new TH1Fs(sampleName+"_MET", "",        30, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   //h_METSig_2sig                = new TH1Fs(sampleName+"_METSig", "",     30, 0, 30,    mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Eta_2sig             = new TH1Fs(sampleName+"_Lepton_Eta", "", 25, -2.5, 2.5, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Pt_2sig             = new TH1Fs(sampleName+"_Lepton_Pt", "",  25, 0, 600,    mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags_2sig                 = new TH1Fs(sampleName+"_nBTags", "",     7, -0.5, 6.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_nJet_2sig                  = new TH1Fs(sampleName+"_nJet", "",      13, -0.5, 12.5,  mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt_2sig                    = new TH1Fs(sampleName+"_Mwt", "",        30,  0, 300,  mySel, m_UncNames, ExpUncertaintyName);
   h_MinDeltaPhiJETMET_2sig      = new TH1Fs(sampleName+"_MinDeltaPhiJETMET", "", 32,  0, 3.2,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_2sig                     = new TH1Fs(sampleName+"_HT", "",        40, 0, 2000,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_bjets_2sig               = new TH1Fs(sampleName+"_HT_bjets", "",  30, 0, 1200,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_2sig                    = new TH1Fs(sampleName+"_mVH", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_HW_2sig            = new TH1Fs(sampleName+"_DeltaPhi_HW", "", 32, 0, 3.2,    mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaEta_HW_2sig            = new TH1Fs(sampleName+"_DeltaEta_HW", "", 55, 0, 5.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_maxMVAResponse_2sig         = new TH1Fs(sampleName+"_maxMVAResponse", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_2sig                    = new TH1Fs(sampleName+"_pTH", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTH_over_mVH_2sig           = new TH1Fs(sampleName+"_pTH_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWplus_2sig                = new TH1Fs(sampleName+"_pTWplus", "",25, 0, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTW_over_mVH_2sig           = new TH1Fs(sampleName+"_pTW_over_mVH", "",25, 0, 1,    mySel, m_UncNames, ExpUncertaintyName);
   h_pTWminus_2sig               = new TH1Fs(sampleName+"_pTWminus", "",25, 0, 800,    mySel, m_UncNames, ExpUncertaintyName);
   h_mH_2sig                     = new TH1Fs(sampleName+"_mH", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName); 
   h_mWplus_2sig                 = new TH1Fs(sampleName+"_mWplus", "",30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_tagCategory_2sig            = new TH1Fs(sampleName+"_BtagCategory", "",11, -0.5, 10.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_mass_resolution_2sig        = new TH1Fs(sampleName+"_mass_resolution", "",20, -1.0, 1.0,    mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_09           = new TH1Fs(sampleName+"_mVH_09", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_29           = new TH1Fs(sampleName+"_mVH_29", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_49           = new TH1Fs(sampleName+"_mVH_49", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_69           = new TH1Fs(sampleName+"_mVH_69", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_07           = new TH1Fs(sampleName+"_mVH_07", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_27           = new TH1Fs(sampleName+"_mVH_27", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_47           = new TH1Fs(sampleName+"_mVH_47", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_67           = new TH1Fs(sampleName+"_mVH_67", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_10           = new TH1Fs(sampleName+"_mVH_10", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_50           = new TH1Fs(sampleName+"_mVH_50", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_52           = new TH1Fs(sampleName+"_mVH_52", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_54          = new TH1Fs(sampleName+"_mVH_54", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_56           = new TH1Fs(sampleName+"_mVH_56", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_19           = new TH1Fs(sampleName+"_mVH_19", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_7            = new TH1Fs(sampleName+"_mVH_7", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8            = new TH1Fs(sampleName+"_mVH_8", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9            = new TH1Fs(sampleName+"_mVH_9", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_95           = new TH1Fs(sampleName+"_mVH_95", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_09_1b          = new TH1Fs(sampleName+"_1B_mVH_09", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_29_1b           = new TH1Fs(sampleName+"_1B_mVH_29", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_49_1b           = new TH1Fs(sampleName+"_1B_mVH_49", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_69_1b           = new TH1Fs(sampleName+"_1B_mVH_69", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_07_1b           = new TH1Fs(sampleName+"_1B_mVH_07", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_27_1b           = new TH1Fs(sampleName+"_1B_mVH_27", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_47_1b          = new TH1Fs(sampleName+"_1B_mVH_47", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_67_1b           = new TH1Fs(sampleName+"_1B_mVH_67", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_10_1b           = new TH1Fs(sampleName+"_1B_mVH_10", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_50_1b           = new TH1Fs(sampleName+"_1B_mVH_50", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_52_1b           = new TH1Fs(sampleName+"_1B_mVH_52", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_54_1b          = new TH1Fs(sampleName+"_1B_mVH_54", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_56_1b           = new TH1Fs(sampleName+"_1B_mVH_56", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_19_1b           = new TH1Fs(sampleName+"_1B_mVH_19", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_mVH_7_1b           = new TH1Fs(sampleName+"_1B_mVH_7", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8_1b           = new TH1Fs(sampleName+"_1B_mVH_8", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9_1b           = new TH1Fs(sampleName+"_1B_mVH_9", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_95_1b           = new TH1Fs(sampleName+"_1B_mVH_95", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   
   h_mVH_09_1c          = new TH1Fs(sampleName+"_1C_mVH_09", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_29_1c           = new TH1Fs(sampleName+"_1C_mVH_29", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_49_1c           = new TH1Fs(sampleName+"_1C_mVH_49", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_69_1c           = new TH1Fs(sampleName+"_1C_mVH_69", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_07_1c           = new TH1Fs(sampleName+"_1C_mVH_07", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_27_1c           = new TH1Fs(sampleName+"_1C_mVH_27", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_47_1c          = new TH1Fs(sampleName+"_1C_mVH_47", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_67_1c           = new TH1Fs(sampleName+"_1C_mVH_67", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_10_1c           = new TH1Fs(sampleName+"_1C_mVH_10", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_50_1c           = new TH1Fs(sampleName+"_1C_mVH_50", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_52_1c          = new TH1Fs(sampleName+"_1C_mVH_52", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_54_1c          = new TH1Fs(sampleName+"_1C_mVH_54", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_56_1c           = new TH1Fs(sampleName+"_1C_mVH_56", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_19_1c           = new TH1Fs(sampleName+"_1C_mVH_19", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_7_1c          = new TH1Fs(sampleName+"_1C_mVH_7", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8_1c          = new TH1Fs(sampleName+"_1C_mVH_8", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9_1c          = new TH1Fs(sampleName+"_1C_mVH_9", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_95_1c           = new TH1Fs(sampleName+"_1C_mVH_95", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);

   h_mVH_09_1l         = new TH1Fs(sampleName+"_1L_mVH_09", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_29_1l           = new TH1Fs(sampleName+"_1L_mVH_29", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_49_1l           = new TH1Fs(sampleName+"_1L_mVH_49", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_69_1l           = new TH1Fs(sampleName+"_1L_mVH_69", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_07_1l           = new TH1Fs(sampleName+"_1L_mVH_07", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_27_1l           = new TH1Fs(sampleName+"_1L_mVH_27", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_47_1l          = new TH1Fs(sampleName+"_1L_mVH_47", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_67_1l           = new TH1Fs(sampleName+"_1L_mVH_67", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_10_1l           = new TH1Fs(sampleName+"_1L_mVH_10", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_50_1l           = new TH1Fs(sampleName+"_1L_mVH_50", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_52_1l          = new TH1Fs(sampleName+"_1L_mVH_52", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_54_1l          = new TH1Fs(sampleName+"_1L_mVH_54", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_56_1l          = new TH1Fs(sampleName+"_1L_mVH_56", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_19_1l           = new TH1Fs(sampleName+"_1L_mVH_19", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   //h_mVH_9_1l         = new TH1Fs(sampleName+"_1L_mVH_9", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_7_1l         = new TH1Fs(sampleName+"_1L_mVH_7", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_8_1l          = new TH1Fs(sampleName+"_1L_mVH_8", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_9_1l         = new TH1Fs(sampleName+"_1L_mVH_9", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);
   h_mVH_95_1l           = new TH1Fs(sampleName+"_1L_mVH_95", "",       35, 0, 3500,    mySel, m_UncNames, ExpUncertaintyName);


   //filling reweighting histograms for different kinematic variables 
   /*h_nJet_1b_rew                = new TH1F(sampleName+"_1B_nJet_rew",sampleName+"_1B_nJet_rew",13, -0.5, 12.5);
   h_HT_1b_rew                  = new TH1F(sampleName+"_1B_HT_rew", sampleName+"_1B_HT_rew",40, 0, 2000);
   h_mVH_1b_rew                 = new TH1F(sampleName+"_1B_mVH_rew", sampleName+"_1B_mVH_rew", 35, 0, 3500);
   h_maxMVAResponse_1b_rew      = new TH1F(sampleName+"_1B_maxMVAResponse_rew", sampleName+"_1B_maxMVAResponse_rew", 20, -1, 1);
   h_pTH_1b_rew                 = new TH1F(sampleName+"_1B_pTH_rew", sampleName+"_1B_pTH_rew",25, 0, 1000);
   h_pTWplus_1b_rew             = new TH1F(sampleName+"_1B_pTWplus_rew", sampleName+"_1B_pTWplus_rew",25, 0, 1000);
   h_pTH_over_mVH_1b_rew        = new TH1F(sampleName+"_1B_pTH_over_mVH_rew", sampleName+"_1B_pTH_over_mVH_rew" ,25, 0, 1);
   h_pTW_over_mVH_1b_rew        = new TH1F(sampleName+"_1B_pTW_over_mVH_rew", sampleName+"_1B_pTW_over_mVH_rew" ,25, 0, 1);
   
   h_nJet_1l_rew                = new TH1F(sampleName+"_1L_nJet_rew",sampleName+"_1L_nJet_rew",13, -0.5, 12.5);
   h_HT_1l_rew                  = new TH1F(sampleName+"_1L_HT_rew", sampleName+"_1L_HT_rew",40, 0, 2000);
   h_mVH_1l_rew                 = new TH1F(sampleName+"_1L_mVH_rew", sampleName+"_1L_mVH_rew", 35, 0, 3500);
   h_maxMVAResponse_1l_rew      = new TH1F(sampleName+"_1L_maxMVAResponse_rew", sampleName+"_1L_maxMVAResponse_rew", 20, -1, 1);
   h_pTH_1l_rew                 = new TH1F(sampleName+"_1L_pTH_rew", sampleName+"_1L_pTH_rew",25, 0, 1000);
   h_pTWplus_1l_rew             = new TH1F(sampleName+"_1L_pTWplus_rew", sampleName+"_1L_pTWplus_rew",25, 0, 1000);
   h_pTH_over_mVH_1l_rew        = new TH1F(sampleName+"_1L_pTH_over_mVH_rew", sampleName+"_1L_pTH_over_mVH_rew",25, 0, 1);
   h_pTW_over_mVH_1l_rew        = new TH1F(sampleName+"_1L_pTW_over_mVH_rew", sampleName+"_1L_pTW_over_mVH_rew",25, 0, 1);

   h_nJet_1c_rew                = new TH1F(sampleName+"_1C_nJet_rew",sampleName+"_1C_nJet_rew",13, -0.5, 12.5);
   h_HT_1c_rew                  = new TH1F(sampleName+"_1C_HT_rew", sampleName+"_1C_HT_rew",40, 0, 2000);
   h_mVH_1c_rew                 = new TH1F(sampleName+"_1C_mVH_rew", sampleName+"_1C_mVH_rew", 35, 0, 3500);
   h_maxMVAResponse_1c_rew      = new TH1F(sampleName+"_1C_maxMVAResponse_rew", sampleName+"_1C_maxMVAResponse_rew", 20, -1, 1);
   h_pTH_1c_rew                 = new TH1F(sampleName+"_1C_pTH_rew", sampleName+"_1C_pTH_rew",25, 0, 1000);
   h_pTWplus_1c_rew             = new TH1F(sampleName+"_1C_pTWplus_rew", sampleName+"_1C_pTWplus_rew",25, 0, 1000);
   h_pTH_over_mVH_1c_rew        = new TH1F(sampleName+"_1C_pTH_over_mVH_rew", sampleName+"_1C_pTH_over_mVH_rew", 25, 0, 1);
   h_pTW_over_mVH_1c_rew        = new TH1F(sampleName+"_1C_pTW_over_mVH_rew", sampleName+"_1C_pTW_over_mVH_rew", 25, 0, 1);*/

   TH1* h_nJet_1b_rew                = new TH1F(sampleName+"_1B_nJet_rew",sampleName+"_1B_nJet_rew",13, -0.5, 12.5);
   TH1* h_HT_1b_rew                  = new TH1F(sampleName+"_1B_HT_rew", sampleName+"_1B_HT_rew",40, 0, 2000);
   TH1* h_mVH_1b_rew                 = new TH1F(sampleName+"_1B_mVH_rew", sampleName+"_1B_mVH_rew", 35, 0, 3500);
   TH1* h_maxMVAResponse_1b_rew      = new TH1F(sampleName+"_1B_maxMVAResponse_rew", sampleName+"_1B_maxMVAResponse_rew", 20, -1, 1);
   TH1* h_pTH_1b_rew                 = new TH1F(sampleName+"_1B_pTH_rew", sampleName+"_1B_pTH_rew",25, 0, 1000);
   TH1* h_pTWplus_1b_rew             = new TH1F(sampleName+"_1B_pTWplus_rew", sampleName+"_1B_pTWplus_rew",25, 0, 1000);
   TH1* h_pTH_over_mVH_1b_rew        = new TH1F(sampleName+"_1B_pTH_over_mVH_rew", sampleName+"_1B_pTH_over_mVH_rew" ,25, 0, 1);
   TH1* h_pTW_over_mVH_1b_rew        = new TH1F(sampleName+"_1B_pTW_over_mVH_rew", sampleName+"_1B_pTW_over_mVH_rew" ,25, 0, 1);
   
   TH1* h_nJet_1l_rew                = new TH1F(sampleName+"_1L_nJet_rew",sampleName+"_1L_nJet_rew",13, -0.5, 12.5);
   TH1* h_HT_1l_rew                  = new TH1F(sampleName+"_1L_HT_rew", sampleName+"_1L_HT_rew",40, 0, 2000);
   TH1* h_mVH_1l_rew                 = new TH1F(sampleName+"_1L_mVH_rew", sampleName+"_1L_mVH_rew", 35, 0, 3500);
   TH1* h_maxMVAResponse_1l_rew      = new TH1F(sampleName+"_1L_maxMVAResponse_rew", sampleName+"_1L_maxMVAResponse_rew", 20, -1, 1);
   TH1* h_pTH_1l_rew                 = new TH1F(sampleName+"_1L_pTH_rew", sampleName+"_1L_pTH_rew",25, 0, 1000);
   TH1* h_pTWplus_1l_rew             = new TH1F(sampleName+"_1L_pTWplus_rew", sampleName+"_1L_pTWplus_rew",25, 0, 1000);
   TH1* h_pTH_over_mVH_1l_rew        = new TH1F(sampleName+"_1L_pTH_over_mVH_rew", sampleName+"_1L_pTH_over_mVH_rew",25, 0, 1);
   TH1* h_pTW_over_mVH_1l_rew        = new TH1F(sampleName+"_1L_pTW_over_mVH_rew", sampleName+"_1L_pTW_over_mVH_rew",25, 0, 1);

   TH1* h_nJet_1c_rew                = new TH1F(sampleName+"_1C_nJet_rew",sampleName+"_1C_nJet_rew",13, -0.5, 12.5);
   TH1* h_HT_1c_rew                  = new TH1F(sampleName+"_1C_HT_rew", sampleName+"_1C_HT_rew",40, 0, 2000);
   TH1* h_mVH_1c_rew                 = new TH1F(sampleName+"_1C_mVH_rew", sampleName+"_1C_mVH_rew", 35, 0, 3500);
   TH1* h_maxMVAResponse_1c_rew      = new TH1F(sampleName+"_1C_maxMVAResponse_rew", sampleName+"_1C_maxMVAResponse_rew", 20, -1, 1);
   TH1* h_pTH_1c_rew                 = new TH1F(sampleName+"_1C_pTH_rew", sampleName+"_1C_pTH_rew",25, 0, 1000);
   TH1* h_pTWplus_1c_rew             = new TH1F(sampleName+"_1C_pTWplus_rew", sampleName+"_1C_pTWplus_rew",25, 0, 1000);
   TH1* h_pTH_over_mVH_1c_rew        = new TH1F(sampleName+"_1C_pTH_over_mVH_rew", sampleName+"_1C_pTH_over_mVH_rew", 25, 0, 1);
   TH1* h_pTW_over_mVH_1c_rew        = new TH1F(sampleName+"_1C_pTW_over_mVH_rew", sampleName+"_1C_pTW_over_mVH_rew", 25, 0, 1);
   //filling reweighting histograms for different kinematic variables

   // Set object pointer
   //mc_generator_weights  = 0;
   el_pt                 = 0;
   el_eta                = 0;
   el_phi                = 0;
   el_e                  = 0;
   //el_cl_eta             = 0;
   el_charge             = 0;
   /*el_topoetcone20       = 0;
   el_ptvarcone20        = 0;
   el_isTight            = 0;
   el_CF                 = 0;
   el_d0sig              = 0;
   el_delta_z0_sintheta                          = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0            = 0;
   el_trigMatch_HLT_e140_lhloose_nod0            = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose   = 0;
   el_LHMedium           = 0;
   el_LHTight            = 0;
   el_isPrompt           = 0;
   el_isoFCLoose         = 0;
   el_isoFCTight         = 0;*/
   mu_pt                 = 0;
   mu_eta                = 0;
   mu_phi                = 0;
   mu_e                  = 0;
   mu_charge             = 0;

   /*mu_d0sig              = 0;
   mu_delta_z0_sintheta  = 0;
   mu_trigMatch_HLT_mu50  = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   mu_Medium              = 0;
   mu_isoFCTightTrackOnly = 0;*/
   /*FatJet_M              = 0;
   FatJet_PT             = 0;
   FatJet_Eta            = 0;
   FatJet_Phi            = 0;*/
   signal_Jet_E          = 0;
   signal_Jet_PT         = 0;
   signal_Jet_Eta        = 0;
   signal_Jet_Phi        = 0;
   signal_Jet_truthflav  = 0;  //only for MC
   signal_Jet_tagWeightBin_DL1r_Continuous = 0;
   

   //only for MC
   
   truth_pt              = 0;
   truth_eta             = 0;
   truth_phi             = 0;
   truth_m               = 0;
   truth_pdgid           = 0; 
   truth_status          = 0;
   truth_tthbb_info      = 0; 

   //only for MC

   weight_bTagSF_DL1r_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_down = 0; 

   //truth_barcode         = 0;
   /*TrackJet_PT           = 0;
   TrackJet_Eta          = 0;
   TrackJet_Phi          = 0;
   TrackJet_M            = 0;
   TrackJet_btagWeight   = 0;*/
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   //std::cout<<"Passed Init 5"<<std::endl;
   fChain->SetBranchAddress("nJets", &nJet, &b_nJet);
   fChain->SetBranchAddress("nMuons", &nMuons, &b_nMuons);
   fChain->SetBranchAddress("nElectrons", &nElectrons, &b_nElectrons);
   fChain->SetBranchAddress("nPrimaryVtx", &nPrimaryVtx, &b_nPrimaryVtx);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   //fChain->SetBranchAddress("nFatJets", &nFatJets, &b_nFatJets);
   fChain->SetBranchAddress("runNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   
   //Below variables only for MC
   
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_normalise", &weight_normalise, &b_weight_normalise);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);

   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);

   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);

   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);

   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);

   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);

   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
  
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);

   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);

   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_up", &weight_bTagSF_DL1r_Continuous_eigenvars_C_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_down", &weight_bTagSF_DL1r_Continuous_eigenvars_C_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_up", &weight_bTagSF_DL1r_Continuous_eigenvars_B_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_down", &weight_bTagSF_DL1r_Continuous_eigenvars_B_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_up", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_down", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down);

   fChain->SetBranchAddress("weight_normalise_fsr_down", &weight_normalise_fsr_down, &b_weight_normalise_fsr_down);
   fChain->SetBranchAddress("weight_normalise_fsr_up", &weight_normalise_fsr_up, &b_weight_normalise_fsr_up);

   fChain->SetBranchAddress("weight_normalise_var3c_down", &weight_normalise_var3c_down, &b_weight_normalise_var3c_down);
   fChain->SetBranchAddress("weight_normalise_var3c_up", &weight_normalise_var3c_up, &b_weight_normalise_var3c_up);

   fChain->SetBranchAddress("weight_normalise_muR20_muF20", &weight_normalise_muR20_muF20, &b_weight_normalise_muR20_muF20);
   fChain->SetBranchAddress("weight_normalise_muR05_muF05", &weight_normalise_muR05_muF05, &b_weight_normalise_muR05_muF05);

   fChain->SetBranchAddress("weight_fsr_down", &weight_fsr_down, &b_weight_fsr_down);
   fChain->SetBranchAddress("weight_fsr_up", &weight_fsr_up, &b_weight_fsr_up);

   fChain->SetBranchAddress("weight_var3c_down", &weight_var3c_down, &b_weight_var3c_down);
   fChain->SetBranchAddress("weight_var3c_up", &weight_var3c_up, &b_weight_var3c_up);

   fChain->SetBranchAddress("weight_muR20_muF20", &weight_muR20_muF20, &b_weight_muR20_muF20);
   fChain->SetBranchAddress("weight_muR05_muF05", &weight_muR05_muF05, &b_weight_muR05_muF05);
   

   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt); 
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP); 
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN); 

    
   fChain->SetBranchAddress("nTaus",      &nTaus,      &b_nTaus);
   //fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("mu", &Mu, &b_Mu);
   //fChain->SetBranchAddress("mu_actual", &ActualMu, &b_ActualMu);
   fChain->SetBranchAddress("met_met", &met, &b_MET);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_MET_phi);
   fChain->SetBranchAddress("dRbb_HiggsMass_85", &dRbb_HiggsMass_85, &b_dRbb_HiggsMass_85);
   fChain->SetBranchAddress("dRbb_MaxM_85", &dRbb_MaxM_85, &b_dRbb_MaxM_85);
   fChain->SetBranchAddress("dRbb_MaxPt_85", &dRbb_MaxPt_85, &b_dRbb_MaxPt_85);
   fChain->SetBranchAddress("dRbb_MindR_85", &dRbb_MindR_85, &b_dRbb_MindR_85);
   fChain->SetBranchAddress("dRbb_avg_85", &dRbb_avg_85, &b_dRbb_avg_85);
   fChain->SetBranchAddress("dRbj_Wmass_85", &dRbj_Wmass_85, &b_dRbj_Wmass_85);
   fChain->SetBranchAddress("Aplanarity_bjets_85", &Aplanarity_bjets_85, &b_Aplanarity_bjets_85);
   fChain->SetBranchAddress("Aplanarity_jets", &Aplanarity_jets, &b_Aplanarity_jets);
   fChain->SetBranchAddress("Centrality_all", &Centrality_all, &b_Centrality_all);
   fChain->SetBranchAddress("Mbj_MaxPt_85", &Mbj_MaxPt_85, &b_Mbj_MaxPt_85);
   fChain->SetBranchAddress("pT_jet3", &pT_jet3, &b_pT_jet3);
   fChain->SetBranchAddress("pT_jet5", &pT_jet5, &b_pT_jet5);
   fChain->SetBranchAddress("pTbb_MindR_85", &pTbb_MindR_85, &b_pTbb_MindR_85);
   fChain->SetBranchAddress("pTuu_MindR_85", &pTuu_MindR_85, &b_pTuu_MindR_85);
   fChain->SetBranchAddress("Mbb_MindR_85", &Mbb_MindR_85, &b_Mbb_MindR_85);
   fChain->SetBranchAddress("Mbb_MinM_85", &Mbb_MinM_85, &b_Mbb_MinM_85);
   fChain->SetBranchAddress("Mbb_MaxPt_85", &Mbb_MaxPt_85, &b_Mbb_MaxPt_85);
   fChain->SetBranchAddress("Mbb_MaxM_85", &Mbb_MaxM_85, &b_Mbb_MaxM_85);
   //fChain->SetBranchAddress("b_mc_generator_weights", &mc_generator_weights,  &b_mc_generator_weights);
   fChain->SetBranchAddress("el_pt",   &el_pt,  &b_el_pt);
   fChain->SetBranchAddress("el_eta",  &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi",  &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e",    &el_e,   &b_el_e);
   //fChain->SetBranchAddress("el_cl_eta",        &el_cl_eta,       &b_el_cl_etdata_2018.root_70p_225_.roota);
   fChain->SetBranchAddress("el_charge",        &el_charge,       &b_el_charge);
   /*fChain->SetBranchAddress("el_topoetcone20",  &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20",   &el_ptvarcone20,  &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_isTight",       &el_isTight,      &b_el_isTight);
   fChain->SetBranchAddress("el_CF",            &el_CF,           &b_el_CF);
   fChain->SetBranchAddress("el_d0sig",         &el_d0sig,        &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta",    &el_delta_z0_sintheta,   &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0",    &el_trigMatch_HLT_e60_lhmedium_nod0,   &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0",    &el_trigMatch_HLT_e140_lhloose_nod0,   &b_el_trigMatch_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose",    &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose,   &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("el_LHMedium",     &el_LHMedium,     &b_el_LHMedium);
   fChain->SetBranchAddress("el_LHTight",      &el_LHTight,      &b_el_LHTight);
   fChain->SetBranchAddress("el_isPrompt",     &el_isPrompt,     &b_el_isPrompt);
   fChain->SetBranchAddress("el_isoFCLoose",   &el_isoFCLoose,   &b_el_isoFCLoose);
   fChain->SetBranchAddress("el_isoFCTight",  &el_isoFCTight,   &b_el_isoFCTight);*/
   fChain->SetBranchAddress("mu_pt",   &mu_pt,  &b_mu_pt);
   fChain->SetBranchAddress("mu_eta",  &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi",  &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e",    &mu_e,   &b_mu_e);
   fChain->SetBranchAddress("mu_charge",    &mu_charge,   &b_mu_charge);
   /*fChain->SetBranchAddress("mu_d0sig",                          &mu_d0sig,   &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta",              &mu_delta_z0_sintheta,   &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu50",             &mu_trigMatch_HLT_mu50,   &b_mu_trigMatch_HLT_mu50);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium",  &mu_trigMatch_HLT_mu26_ivarmedium,   &b_mu_trigMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("mu_Medium",                         &mu_Medium,   &b_mu_Medium);
   fChain->SetBranchAddress("mu_isoFCTightTrackOnly",            &mu_isoFCTightTrackOnly,   &b_mu_isoFCTightTrackOnly);*/
   /*fChain->SetBranchAddress("FatJet_M", &FatJet_M, &b_FatJet_M);
   fChain->SetBranchAddress("FatJet_PT", &FatJet_PT, &b_FatJet_PT);
   fChain->SetBranchAddress("FatJet_Eta", &FatJet_Eta, &b_FatJet_Eta);
   fChain->SetBranchAddress("FatJet_Phi", &FatJet_Phi, &b_FatJet_Phi);*/
   fChain->SetBranchAddress("jet_e",   &signal_Jet_E, &b_signal_Jet_E);
   fChain->SetBranchAddress("jet_pt",  &signal_Jet_PT, &b_signal_Jet_PT);
   fChain->SetBranchAddress("jet_eta", &signal_Jet_Eta, &b_signal_Jet_Eta);
   fChain->SetBranchAddress("jet_phi", &signal_Jet_Phi, &b_signal_Jet_Phi);
   fChain->SetBranchAddress("jet_truthflav", &signal_Jet_truthflav, &b_signal_Jet_truthflav); //only for MC
   fChain->SetBranchAddress("jet_tagWeightBin_DL1r_Continuous", &signal_Jet_tagWeightBin_DL1r_Continuous, &b_signal_Jet_tagWeightBin_DL1r_Continuous);

   //Below variables only for MC
   fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
   fChain->SetBranchAddress("truth_eta", &truth_eta, &b_truth_eta);
   fChain->SetBranchAddress("truth_phi", &truth_phi, &b_truth_phi);
   fChain->SetBranchAddress("truth_m", &truth_m, &b_truth_m);
   fChain->SetBranchAddress("truth_pdgid", &truth_pdgid, &b_truth_pdgid);
   fChain->SetBranchAddress("truth_status", &truth_status, &b_truth_status);
   fChain->SetBranchAddress("truth_tthbb_info", &truth_tthbb_info, &b_truth_tthbb_info); 
   fChain->SetBranchAddress("HF_SimpleClassification", &HF_SimpleClassification, &b_HF_SimpleClassification);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("HT_all", &HT_all, &b_HT_all);
   fChain->SetBranchAddress("TopHeavyFlavorFilterFlag", &TopHeavyFlavorFilterFlag, &b_TopHeavyFlavorFilterFlag);
   //fChain->SetBranchAddress("truth_barcode", &truth_barcode, &b_truth_barcode);
   /*fChain->SetBranchAddress("TrackJet_PT",&TrackJet_PT, &b_TrackJet_PT);
   fChain->SetBranchAddress("TrackJet_Eta",&TrackJet_Eta, &b_TrackJet_Eta);
   fChain->SetBranchAddress("TrackJet_Phi", &TrackJet_Phi, &b_TrackJet_Phi);
   fChain->SetBranchAddress("TrackJet_M", &TrackJet_M, &b_TrackJet_M);
   fChain->SetBranchAddress("TrackJet_btagWeight", &TrackJet_btagWeight, &b_TrackJet_btagWeight);*/
   Notify();
   //std::cout<<"Passed Init 6"<<std::endl;
   m_myTree_hadronic  = new TTree("MVATree_bbqq","");
   m_myTree_hadronic->Branch("btagjH1",          &m_btagjH1);
   m_myTree_hadronic->Branch("btagjH2",          &m_btagjH2);
   m_myTree_hadronic->Branch("btagjW1",          &m_btagjWp1);
   m_myTree_hadronic->Branch("btagjW2",          &m_btagjWp2);
   m_myTree_hadronic->Branch("H_mass",           &m_H_mass);
   m_myTree_hadronic->Branch("Wp_mass",          &m_Wp_mass);
   m_myTree_hadronic->Branch("H_pT",             &m_H_pT);
   m_myTree_hadronic->Branch("Wp_pT",            &m_Wp_pT);
   m_myTree_hadronic->Branch("pTjH1",            &m_pTjH1);
   m_myTree_hadronic->Branch("pTjH2",            &m_pTjH2);
   m_myTree_hadronic->Branch("pTjWp1",           &m_pTjWp1);
   m_myTree_hadronic->Branch("pTjWp2",           &m_pTjWp2);
   m_myTree_hadronic->Branch("dRjjH",            &m_dRjjH);
   m_myTree_hadronic->Branch("dRjjWp",           &m_dRjjWp);
   m_myTree_hadronic->Branch("Phi_HW",           &m_Phi_HW);
   m_myTree_hadronic->Branch("Eta_HW",           &m_Eta_HW);
   m_myTree_hadronic->Branch("dR_HW",            &m_dR_HW);
   m_myTree_hadronic->Branch("mass_VH",          &m_mass_VH);
   m_myTree_hadronic->Branch("is_Signal",        &m_is_Signal); 
   m_myTree_hadronic->Branch("cos_thetastar",        &m_costhetastar); 
   //m_myTree_hadronic->Branch("eventNumber",        &jentry);
   m_myTree_hadronic->Branch("EventWeight",	 &EventWeight);

   m_myTree_Leptonic  = new TTree("MVATree_lvqq","");
   m_myTree_Leptonic->Branch("btagjH1",          &m_btagjH1);
   m_myTree_Leptonic->Branch("btagjH2",          &m_btagjH2);
   m_myTree_Leptonic->Branch("H_mass",           &m_H_mass);
   m_myTree_Leptonic->Branch("Wp_mass",          &m_Wp_mass);
   m_myTree_Leptonic->Branch("H_pT",             &m_H_pT);
   m_myTree_Leptonic->Branch("Wp_pT",            &m_Wp_pT);
   m_myTree_Leptonic->Branch("pTjH1",            &m_pTjH1);
   m_myTree_Leptonic->Branch("pTjH2",            &m_pTjH2);
   m_myTree_Leptonic->Branch("pTjWp1",           &m_pTjWp1);
   m_myTree_Leptonic->Branch("pTjWp2",           &m_pTjWp2);
   m_myTree_Leptonic->Branch("dRjjH",            &m_dRjjH);
   m_myTree_Leptonic->Branch("dRjjWp",           &m_dRjjWp);
   m_myTree_Leptonic->Branch("Phi_HW",           &m_Phi_HW);
   m_myTree_Leptonic->Branch("Eta_HW",           &m_Eta_HW);
   m_myTree_hadronic->Branch("dR_HW",            &m_dR_HW);
   m_myTree_Leptonic->Branch("mass_VH",          &m_mass_VH);
   m_myTree_Leptonic->Branch("is_Signal",        &m_is_Signal);
   m_myTree_Leptonic->Branch("EventWeight",      &EventWeight);
  //std::cout<<"Passed Init 7"<<std::endl;
   m_myTree_Event = new TTree("MVATree","");
   m_myTree_Event->Branch("is_Signal",         &m_is_Signal);
   ///m_myTree_Event->Branch("EventWeight",       &EventWeight); /// comment in once fixed !!!!!
   m_myTree_Event->Branch("Lep_pT",            &m_Lep_pT);
   m_myTree_Event->Branch("Lep_Eta",           &m_Lep_Eta);
   m_myTree_Event->Branch("MET",               &m_MET);
   m_myTree_Event->Branch("HT6j",              &m_HT6j);
   ////m_myTree_Event->Branch("bTagScoreSum4j",    &m_bTagScoreSum4j);
   m_myTree_Event->Branch("bTagScoreSum6j",    &m_bTagScoreSum6j);
   ///m_myTree_Event->Branch("dR_lj_max",         &m_dR_lj_max);
   m_myTree_Event->Branch("dR_lj_min",         &m_dR_lj_min);
   m_myTree_Event->Branch("dRbb_HiggsMass_85", &dRbb_HiggsMass_85);
   ///m_myTree_Event->Branch("dRbb_MaxM_85",      &dRbb_MaxM_85);
   m_myTree_Event->Branch("dRbb_MaxPt_85",     &dRbb_MaxPt_85);
   m_myTree_Event->Branch("dRbb_MindR_85",     &dRbb_MindR_85);
   ///m_myTree_Event->Branch("dRbb_avg_85",       &dRbb_avg_85);
   ///m_myTree_Event->Branch("dRbj_Wmass_85",         &dRbj_Wmass_85);
   m_myTree_Event->Branch("Aplanarity_bjets_85",   &Aplanarity_bjets_85);
   ///m_myTree_Event->Branch("Aplanarity_jets",       &Aplanarity_jets);
   ///m_myTree_Event->Branch("Centrality_all",  	   &Centrality_all);
   m_myTree_Event->Branch("Mbj_MaxPt_85",  	   &Mbj_MaxPt_85);
   m_myTree_Event->Branch("pT_jet3",  	           &pT_jet3);
   m_myTree_Event->Branch("pT_jet5",  	           &pT_jet5);
   m_myTree_Event->Branch("pTbb_MindR_85",  	   &pTbb_MindR_85);
   m_myTree_Event->Branch("pTuu_MindR_85",  	   &pTuu_MindR_85);
   ///m_myTree_Event->Branch("Mbb_MindR_85",  	   &Mbb_MindR_85);
   ///m_myTree_Event->Branch("Mbb_MinM_85",	   &Mbb_MinM_85);
   ///m_myTree_Event->Branch("Mbb_MaxPt_85",	   &Mbb_MaxPt_85);
   m_myTree_Event->Branch("Mbb_MaxM_85",	   &Mbb_MaxM_85);

   Nom = new TTree("Nom","");
   Nom->Branch("massWH",&massWH);
   Nom->Branch("nJets",&nJets);
   Nom->Branch("maxMVA",&maxMVA);
   Nom->Branch("pTWplus",&pTWplus);
   Nom->Branch("pTHiggs",&pTHiggs);
   Nom->Branch("nBTags",&nBTags);
   Nom->Branch("HFClass",&HFClass);
   Nom->Branch("mcChanNbr",&mcChanNbr);
   Nom->Branch("TopHeavyFF",&TopHeavyFF);
   Nom->Branch("HT",&HT);
   Nom->Branch("ttFlag",&ttFlag);
   //Nominal->Branch("JetPairqqbb",&JetPairqqbb);
   Nom->Branch("EvtWeight_wott",&EvtWeight_wott);
   Nom->Branch("EvtWeight_wtt",&EvtWeight_wtt);
   Nom->Branch("DelEtaHW",&DelEtaHW);
   Nom->Branch("DelPhiHW",&DelPhiHW);
   //Nom->Branch("HT_bjet",&HT_bjet);
   Nom->Branch("pTHiggs_over_massWH",&pTHiggs_over_massWH);
   Nom->Branch("pTj1H",&pTj1H);
   Nom->Branch("pTj2H",&pTj2H);
   Nom->Branch("pTj1W",&pTj1W);
   Nom->Branch("pTj2W",&pTj2W);
   Nom->Branch("etaj1H",&etaj1H);
   Nom->Branch("etaj2H",&etaj2H);
   Nom->Branch("etaj1W",&etaj1W);
   Nom->Branch("etaj2W",&etaj2W);
   Nom->Branch("pTWplus_over_massWH",&pTWplus_over_massWH);
   Nom->Branch("massHiggs",&massHiggs);
   Nom->Branch("massWplus",&massWplus);
   Nom->Branch("btagj1H", &btagj1H);
   Nom->Branch("btagj2H", &btagj2H);
   Nom->Branch("btagj1W", &btagj1W);
   Nom->Branch("btagj2W", &btagj2W);
   Nom->Branch("Mwt",&Mwt);
   
   
   Nom->Branch("weight_normal",&weight_normal);
   Nom->Branch("weight_MC",&weight_MC);
   Nom->Branch("weight_btagSF",&weight_btagSF);
   Nom->Branch("weight_normal_mu2f2",&weight_normal_mu2f2);
   Nom->Branch("weight_normal_mu5f5",&weight_normal_mu5f5);
   Nom->Branch("weight_normal_fsrUP",&weight_normal_fsrUP);
   Nom->Branch("weight_normal_fsrDOWN",&weight_normal_fsrDOWN);
   Nom->Branch("weight_normal_varUP",&weight_normal_varUP);
   Nom->Branch("weight_normal_varDOWN",&weight_normal_varDOWN);
   Nom->Branch("weight_mu2f2",&weight_mu2f2);
   Nom->Branch("weight_mu5f5",&weight_mu5f5);
   Nom->Branch("weight_fsrUP",&weight_fsrUP);
   Nom->Branch("weight_fsrDOWN",&weight_fsrDOWN);
   Nom->Branch("weight_varUP",&weight_varUP);
   Nom->Branch("weight_varDOWN",&weight_varDOWN);

   Nom->Branch("weight_btagSF_CUP",&weight_btagSF_CUP);
   Nom->Branch("weight_btagSF_CDOWN",&weight_btagSF_CDOWN);
   Nom->Branch("weight_btagSF_BUP",&weight_btagSF_BUP);
   Nom->Branch("weight_btagSF_BDOWN",&weight_btagSF_BDOWN);
   Nom->Branch("weight_btagSF_LUP",&weight_btagSF_LUP);
   Nom->Branch("weight_btagSF_LDOWN",&weight_btagSF_LDOWN);

   Nom->Branch("weight_pu",&weight_pu);
   Nom->Branch("weight_jetvt",&weight_jetvt);
   Nom->Branch("weight_lepSF",&weight_lepSF);

   Nom->Branch("weight_pu_UP",&weight_pu_UP);
   Nom->Branch("weight_pu_DOWN",&weight_pu_DOWN);
   Nom->Branch("weight_jetvt_UP",&weight_jetvt_UP);
   Nom->Branch("weight_jetvt_DOWN",&weight_jetvt_DOWN);

   Nom->Branch("weight_lepSF_mu_TTVA_Syst_UP",&weight_lepSF_mu_TTVA_Syst_UP);
   Nom->Branch("weight_lepSF_mu_TTVA_Syst_DOWN",&weight_lepSF_mu_TTVA_Syst_DOWN);
   Nom->Branch("weight_lepSF_mu_TTVA_Stat_UP",&weight_lepSF_mu_TTVA_Stat_UP);
   Nom->Branch("weight_lepSF_mu_TTVA_Stat_DOWN",&weight_lepSF_mu_TTVA_Stat_DOWN);

   Nom->Branch("weight_lepSF_mu_Trigger_Syst_UP",&weight_lepSF_mu_Trigger_Syst_UP);
   Nom->Branch("weight_lepSF_mu_Trigger_Syst_DOWN",&weight_lepSF_mu_Trigger_Syst_DOWN);
   Nom->Branch("weight_lepSF_mu_Trigger_Stat_UP",&weight_lepSF_mu_Trigger_Stat_UP);
   Nom->Branch("weight_lepSF_mu_Trigger_Stat_DOWN",&weight_lepSF_mu_Trigger_Stat_DOWN);

   Nom->Branch("weight_lepSF_mu_ID_Syst_UP",&weight_lepSF_mu_ID_Syst_UP);
   Nom->Branch("weight_lepSF_mu_ID_Syst_DOWN",&weight_lepSF_mu_ID_Syst_DOWN);
   Nom->Branch("weight_lepSF_mu_ID_Stat_UP",&weight_lepSF_mu_ID_Stat_UP);
   Nom->Branch("weight_lepSF_mu_ID_Stat_DOWN",&weight_lepSF_mu_ID_Stat_DOWN);

   Nom->Branch("weight_lepSF_mu_ID_lowpt_Syst_UP",&weight_lepSF_mu_ID_lowpt_Syst_UP);
   Nom->Branch("weight_lepSF_mu_ID_lowpt_Syst_DOWN",&weight_lepSF_mu_ID_lowpt_Syst_DOWN);
   Nom->Branch("weight_lepSF_mu_ID_lowpt_Stat_UP",&weight_lepSF_mu_ID_lowpt_Stat_UP);
   Nom->Branch("weight_lepSF_mu_ID_lowpt_Stat_DOWN",&weight_lepSF_mu_ID_lowpt_Stat_DOWN);

   Nom->Branch("weight_lepSF_mu_Isol_Syst_UP",&weight_lepSF_mu_Isol_Syst_UP);
   Nom->Branch("weight_lepSF_mu_Isol_Syst_DOWN",&weight_lepSF_mu_Isol_Syst_DOWN);
   Nom->Branch("weight_lepSF_mu_Isol_Stat_UP",&weight_lepSF_mu_Isol_Stat_UP);
   Nom->Branch("weight_lepSF_mu_Isol_Stat_DOWN",&weight_lepSF_mu_Isol_Stat_DOWN);

   Nom->Branch("weight_lepSF_el_Isol_UP",&weight_lepSF_el_Isol_UP);
   Nom->Branch("weight_lepSF_el_Isol_DOWN",&weight_lepSF_el_Isol_DOWN);

   Nom->Branch("weight_lepSF_el_ID_UP",&weight_lepSF_el_ID_UP);
   Nom->Branch("weight_lepSF_el_ID_DOWN",&weight_lepSF_el_ID_DOWN);

   Nom->Branch("weight_lepSF_el_Reco_UP",&weight_lepSF_el_Reco_UP);
   Nom->Branch("weight_lepSF_el_Reco_DOWN",&weight_lepSF_el_Reco_DOWN);

   Nom->Branch("weight_lepSF_el_Trigger_UP",&weight_lepSF_el_Trigger_UP);
   Nom->Branch("weight_lepSF_el_Trigger_DOWN",&weight_lepSF_el_Trigger_DOWN); 
   

   //Nominal->Branch("MC16_scale",&MC16_scale);
   //std::cout<<"Passed Init 8"<<std::endl;
}

Bool_t EventLoop::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EventLoop::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EventLoop::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef EventLoop_cxx
